import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import {Provider as ReduxProvider} from 'react-redux';

import {PrivateRoute} from 'ui';
import App from './features/App';
import Auth from './features/auth/auth';
import registerServiceWorker from './libs/register-service-worker';
import {routes} from 'routes';
import {configureStore} from "store/index";

const store = configureStore();

ReactDOM.render(
        <ReduxProvider store={store}>
            <BrowserRouter>
                <Switch>
                    <Route path={routes.auth.path} component={Auth}/>
                    <PrivateRoute path={routes.root.path} component={App}/>
                </Switch>
            </BrowserRouter>
        </ReduxProvider>
    , document.getElementById('root'));
registerServiceWorker();
