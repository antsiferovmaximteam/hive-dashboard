export const routes = {
    root: {
        path: '/',
        applications: {
            path: '/applications',
            application: {
                path: '/applications/:id',
                settings: {
                    path: '/applications/:id/settings'
                },
                scopes: {
                    path: '/applications/:id/scopes'
                }
            }
        },
        logs: {
            path: '/logs'
        },
        fraudPrevention: {
            path: '/fraud-prevention'
        },
        analytics: {
            path: '/analytics'
        },
        account: {
            path: '/account'
        },
    },
    auth: {
        path: '/auth',
        login: {
            path: '/auth/login'
        },
        registration: {
            path: '/auth/registration'
        },
        account: {
            path: '/auth/account'
        }
    }
};
