export function stateToLocalStorage(name, state, keys) {
    const data = keys.reduce((acc, key) => ({...acc, [key]: state[key]}), {});

    localStorage.setItem(name, JSON.stringify(data));
    
    return state;
}