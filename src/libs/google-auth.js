export class GoogleAuth {
    static authorize() {
        return new Promise((resolve, reject) => {
            window.gapi.auth2.authorize({
                client_id: process.env.REACT_APP_GOOGLE_CLIENT_ID,
                scope: 'email profile openid',
                response_type: 'id_token permission'
            }, async function(response) {
                response.error ? reject(response.error) : resolve(response.id_token);
            });
        });
    }
}