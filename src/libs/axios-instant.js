import a from 'axios';
import btoa from 'btoa';
import {stateToLocalStorage} from 'libs/state-to-local-storage';
import {secondsToMilliseconds} from 'libs/time';
import {Api} from 'api';

const API_URL = process.env.REACT_APP_API_URL || '';
const config = { baseURL: API_URL };

export const axiosBasicAuth = a.create(config);
const axios = a.create(config);

axiosBasicAuth.interceptors.request.use(async function (config) {
    let authData = localStorage.getItem('auth');
    authData = authData ? JSON.parse(authData) : null;

    if (authData && !authData.access_token) {
        localStorage.removeItem('auth');
    }

    const client_id = await Api.getClientId();

    const headers = {
        'X-Requested-With': 'XMLHttpRequest',
        'Authorization': 'Basic ' + btoa(`${client_id}:`)
    };

    return {
        ...config, headers: {
            ...config.headers,
            ...headers
        }
    };
});

axiosBasicAuth.interceptors.response.use(async function (res) {
   return res; 
}, function (error) {
    const code = getErrorCode(error.message);

    if (code === 401 || code === 400) {
        clearAuthData()
    }
});

axios.interceptors.request.use(async function (config) {
    let {access_token, refresh_token, expires_in} = JSON.parse(localStorage.getItem('auth'));
    const minExpiredTime = 100; // milliseconds

    if (expires_in - Date.now() < minExpiredTime) {
        try {
            access_token = await refreshToken(refresh_token);
        } catch (e) {
            const code = getErrorCode(e.message);

            if (code === 401) {
                clearAuthData()
            }
        }
    }

    const headers = {...config.headers, Authorization: 'Bearer ' + access_token};

    return {...config, headers};
});

async function refreshToken(refreshToken) {
    const {data} = await Api.refreshToken(refreshToken);

    stateToLocalStorage('auth', {
        ...data,
        expires_in: Date.now() + secondsToMilliseconds(data.expires_in)
    }, ['access_token', 'refresh_token', 'expires_in']);

    return data.access_token;
}

function clearAuthData() {
    localStorage.removeItem('auth');

    window.location.href = '/'
}

function getErrorCode(message) {
    return +message.split('code')[1];
}

export default axios;
