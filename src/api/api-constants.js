export const url = {
    token: '/oauth/token',
    clientId: '/clients',
    registration: '/users',
    registrationWithGoogle: '/users/google',
    loginWithGoogle: '/users/login_google',
    logout: '/oauth/revoke',
    user: '/users/me',
    account: '/accounts',
    apps: '/apps'
};