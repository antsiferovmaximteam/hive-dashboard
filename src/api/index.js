import axios, {axiosBasicAuth} from 'libs/axios-instant';
import pureAxios from 'axios';
import {url} from './api-constants';
import {randomString} from 'libs/random-string';
import acmeLogo from 'assets/images/acme-logo.svg';
import defaultLogo from 'assets/images/default-logo.svg';

class Api {
    static async getToken(email, password) {
        const params = new URLSearchParams();
        params.append('grant_type', 'password');
        params.append('username', email);
        params.append('password', password);
        params.append('scope', '*');

        /*return axiosBasicAuth(
            {
                method: 'POST',
                url: url.token,
                data: params.toString(),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                }
            }
        );*/

        return {
            status: 200,
            data: {
                access_token: 'access_token',
                refresh_token: 'refresh_token'
            }
        }
    }

    static refreshToken(refresh_token) {
        const params = new URLSearchParams();
        params.append('grant_type', 'refresh_token');
        params.append('refresh_token', refresh_token);
        params.append('scope', '*');

        return axiosBasicAuth.post(url.token, params.toString());
    }

    static getUser() {
        return axios.get(url.user)
    }

    static async getClientId() {
        const client_id = localStorage.getItem('client_id');

        if (client_id) {
            return client_id;
        } else {
            const {data} = await pureAxios.post(url.clientId);

            localStorage.setItem('client_id', data.client_id);
            return data.client_id;
        }
    }

    static registration(name, email, password) {
        return axiosBasicAuth.post(url.registration, {
            name, email, password
        })
    }

    static async registrationWithGoogle(token) {
        const client_id = await Api.getClientId();

        return axiosBasicAuth.post(url.registrationWithGoogle, {
            token,
            client_id
        })
    }

    static async loginWithGoogle(token) {
        const client_id = await Api.getClientId();

        return pureAxios.post(url.loginWithGoogle, {
            token,
            client_id
        });
    }

    static logout() {
        return axios.post(url.logout);
    }

    static createAccount(payload) {
        return axios.post(url.account, payload);
    }

    static async getAccount() {
        const res = await axios.get(url.account);

        return {
            ...res,
            data: res.data[0]
        }
    }

    static getApplications() {
        return axios.get(url.apps);
    }

    static updateApplication(payload, appId) {
        return axios.patch(`${url.apps}/${appId}`, payload);
    }

    static createApplication(payload) {
        return axios.post(url.apps, payload);
    }
}

const APPS_LOCAL_STORAGE_KEY = 'APPS_LOCAL_STORAGE_KEY';

const res = localStorage.getItem(APPS_LOCAL_STORAGE_KEY);

const apps = res ? JSON.parse(res) : [{
    name: 'ACME',
    app_id: 'vjj2wIUmZEjVyA9xWA7oyFhmaihdJAUNuLOcwNtG',
    secret: 'GAL5KUWHZEDVW4gQVXgaTE1gRXPzHe7wKxDj9fzn',
    description: 'This is just a short description of the application',
    allowed_callback_urls: ['https://auth.hive.id/callback'],
    allowed_origins: ['*'],
    allowed_cors: ['*'],
    application_login_uri: 'https://auth.hive.id/callback',
    logo: acmeLogo,
    privacy_policy: 'Looney Tunes built the ACME app as an Open Source app. This SERVICE is provided by Looney Tunes at no cost and is intended for use as is.\n' +
        'This page is used to inform visitors regarding our policies with the collection, use, and disclosure of Personal Information if anyone decided to use our Service.\n' +
        'If you choose to use our Service, then you agree to the collection and use of information in relation to this policy. The Personal Information that we collect is used for providing and improving the Service. We will not use or share your information with anyone except as described in this Privacy Policy.\n' +
        'The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is accessible at ACME unless otherwise defined in this Privacy Policy.',
    terms_of_use: 'By downloading or using the app, these terms will automatically apply to you – you should make sure therefore that you read them carefully before using the app. You’re not allowed to copy, or modify the app, any part of the app, or our trademarks in any way. You’re not allowed to attempt to extract the source code of the app, and you also shouldn’t try to translate the app into other languages, or make derivative versions. The app itself, and all the trade marks, copyright, database rights and other intellectual property rights related to it, still belong to Looney Tunes.\n' +
        'Looney Tunes is committed to ensuring that the app is as useful and efficient as possible. For that reason, we reserve the right to make changes to the app or to charge for its services, at any time and for any reason. We will never charge you for the app or its services without making it very clear to you exactly what you’re paying for.\n' +
        'The ACME app stores and processes personal data that you have provided to us, in order to provide our Service. It’s your responsibility to keep your phone and access to the app secure. We therefore recommend that you do not jailbreak or root your phone, which is the process of removing software restrictions and limitations imposed by the official operating system of your device. It could make your phone vulnerable to malware/viruses/malicious programs, compromise your phone’s security features and it could mean that the ACME app won’t work properly or at all.'
}];

!res && localStorage.setItem(APPS_LOCAL_STORAGE_KEY, JSON.stringify(apps));

class FakeApi extends Api {
    static user = {
        name: 'Yuriy Znatokov',
        avatar: 'https://cdn4.iconfinder.com/data/icons/user-avatar-flat-icons/512/User_Avatar-04-512.png'
    };

    static async getUser() {
        return {
            status: 200,
            data: FakeApi.user
        };
    }

    static async getClientId() {
        return {
            status: 200,
            data: {
                clientId: 'dsasadsda'
            }
        }
    }

    static async refreshToken() {
        return {
            status: 201,
            data: {
                access_token: 'sdasdasdasda',
                refresh_token: 'sadsdasda',
                expires_in: 1334212
            }
        }
    }

    static async getApplications() {
        return {
            status: 200,
            data: apps
        }
    }

    static async createApplication(payload) {
        const app = {
            ...payload,
            app_id: randomString(40),
            secret: randomString(40),
            description: '',
            allowed_callback_urls: [''],
            allowed_origins: ['*'],
            allowed_cors: ['*'],
            application_login_uri: '',
            logo: defaultLogo,
            privacy_policy: '',
            terms_of_use: ''
        };

        const apps = JSON.parse(localStorage.getItem(APPS_LOCAL_STORAGE_KEY));
        localStorage.setItem(APPS_LOCAL_STORAGE_KEY, JSON.stringify([...apps, app]));

        return {
            status: 200,
            data: app
        };
    }

    static async updateApplication(payload, appId) {
        const app = {
            description: 'Description',
            allowed_origins: ['*'],
            allowed_cors: ['*'],
            logo: 'https://www.acmebrooklyn.com/wp-content/uploads/2018/06/ACME-Logo.png',
            app_id: appId,
            ...payload,
        };

        const apps = JSON.parse(localStorage.getItem(APPS_LOCAL_STORAGE_KEY));

        localStorage.setItem(APPS_LOCAL_STORAGE_KEY, JSON.stringify(apps.map(item => item.app_id === appId ? app : item)));

        return {
            status: 201,
            data: app
        };
    }
}

export {FakeApi as Api}
