import React from 'react';

import {NavIcon} from './nav-icon';

export const LogsIcon = () => (
 <NavIcon>
   <path d="M2,1 L14,1 C14.5522847,1 15,1.44771525 15,2 L15,14 C15,14.5522847 14.5522847,15 14,15 L2,15 C1.44771525,15 1,14.5522847 1,14 L1,2 C1,1.44771525 1.44771525,1 2,1 Z M10,12 L11.5,12 L11.5,10 L10,10 L10,12 Z M7.25,12 L8.75,12 L8.75,4 L7.25,4 L7.25,12 Z M4.5,12 L6,12 L6,7 L4.5,7 L4.5,12 Z"/>
 </NavIcon>
);