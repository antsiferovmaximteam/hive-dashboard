import React from 'react';
import styled from 'styled-components';

const Svg = styled('svg')`
  margin-top: 3px;
  margin-right: 15px;
`;

export const NavIcon = ({children}) => (
 <Svg width="16px" height="16px" viewBox="0 0 16 16" version="1.1">
   <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
     <g fill="currentColor">
       {children}
     </g>
   </g>
 </Svg>
);