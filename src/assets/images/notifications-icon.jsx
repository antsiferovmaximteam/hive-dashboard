import React from 'react';
import styled from 'styled-components';

const IconWrapper = styled('div')`
  position: relative;

  &:after {
    content: '';
    position: absolute;
    top: -3px;
    right: -3px;
    display: block;
    height: 6px;
    width: 6px;
    background-color: #7956EF;
    border-radius: 50%;
    border: 2px solid white;
  }
`;

export const NotificationsIcon = () => (
 <IconWrapper>
   <svg width="12px" height="14px" viewBox="0 0 12 14" version="1.1" xmlns="http://www.w3.org/2000/svg">
     <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
       <g transform="translate(-1088.000000, -15.000000)" fill="#767FAB">
         <g transform="translate(1088.000000, 12.000000)">
           <path d="M7.26044312,15.1524219 C7.3062124,15.2874118 7.33099168,15.4317807 7.33099168,15.5818177 C7.33099168,16.3337991 6.70853887,16.9434005 5.94070441,16.9434005 C5.17286996,16.9434005 4.55041715,16.3337991 4.55041715,15.5818177 C4.55041715,15.4317807 4.57519642,15.2874118 4.6209657,15.1524219 L0.0133313892,15.1524219 L0.0133313892,14.4888763 C0.32003534,14.2076864 0.65020635,13.8429494 0.914655085,13.3893371 C1.90984702,11.67069 1.32933653,10.7182818 1.72051464,7.53841343 C2.07023548,4.68800895 4.253071,3.87200412 5.372671,3.63905271 L6.51026119,3.63905271 C7.62830155,3.87200412 9.81113707,4.68804447 10.1616559,7.53841343 C10.552834,10.7197737 9.97232349,11.67069 10.9675154,13.3893371 C11.2304045,13.8429494 11.5621352,14.2076864 11.8680412,14.4888763 L11.8680412,15.1524219 L7.26044312,15.1524219 Z"/>
         </g>
       </g>
     </g>
   </svg>
 </IconWrapper>
);