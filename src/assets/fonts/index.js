import LatoBlackEOT from './lato-black.eot';
import LatoBlackWOFF2 from './lato-black.woff2';
import LatoBlackWOFF from './lato-black.woff';
import LatoBlackSVG from './lato-black.svg';

import LatoBoldEOT from './lato-bold.eot';
import LatoBoldWOFF2 from './lato-bold.woff2';
import LatoBoldWOFF from './lato-bold.woff';
import LatoBoldSVG from './lato-bold.svg';

import LatoSemiboldEOT from './lato-semibold.eot';
import LatoSemiboldWOFF2 from './lato-semibold.woff2';
import LatoSemiboldWOFF from './lato-semibold.woff';
import LatoSemiboldSVG from './lato-semibold.svg';

import LatoRegularEOT from './lato-regular.eot';
import LatoRegularWOFF2 from './lato-regular.woff2';
import LatoRegularWOFF from './lato-regular.woff';
import LatoRegularSVG from './lato-regular.svg';

import LatoLightEOT from './lato-light.eot';
import LatoLightWOFF2 from './lato-light.woff2';
import LatoLightWOFF from './lato-light.woff';
import LatoLightSVG from './lato-light.svg';

const fonts = [
  {
    eot: LatoBlackEOT,
    woff2: LatoBlackWOFF2,
    woff: LatoBlackWOFF,
    svg: LatoBlackSVG,
    weight: 900
  },
  {
    eot: LatoBoldEOT,
    woff2: LatoBoldWOFF2,
    woff: LatoBoldWOFF,
    svg: LatoBoldSVG,
    weight: 700
  },
  {
    eot: LatoSemiboldEOT,
    woff2: LatoSemiboldWOFF2,
    woff: LatoSemiboldWOFF,
    svg: LatoSemiboldSVG,
    weight: 600
  },
  {
    eot: LatoRegularEOT,
    woff2: LatoRegularWOFF2,
    woff: LatoRegularWOFF,
    svg: LatoRegularSVG,
    weight: 400
  },
  {
    eot: LatoLightEOT,
    woff2: LatoLightWOFF2,
    woff: LatoLightWOFF,
    svg: LatoLightSVG,
    weight: 300
  },
];

export {fonts as Fonts};
