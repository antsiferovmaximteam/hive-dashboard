import React from 'react';
import styled from 'styled-components';

import {WhiteCardWithTitle, LineChart} from 'ui';
import moment from "moment";
import ReferenceLine from "recharts/lib/cartesian/ReferenceLine";
import {getRandomInt} from "../../../../libs/get-random-int";

const Container = styled('div')`
  display: flex;
`;

const Stats = styled('h3')`
  margin-top: 6px;
  font-weight: bold;
  font-size: 32px;
  color: #3E466D;
  letter-spacing: 0;
  line-height: 48px;
`;

const Description = styled('p')`
  font-weight: 600;
  font-size: 14px;
  color: #504A8E;
  letter-spacing: 0;
  line-height: 22px;
  text-transform: uppercase;
`;

const Left = styled('div')`
  display: flex;
  flex-direction: column;
  width: 50%;
`;

const ChartContainer = styled('div')`
  position: relative;
  width: 50%;
`;

const cardTicks = [
    // moment().subtract(3, 'months').unix(),
    // moment().subtract(2, 'months').unix(),
    moment().subtract(1, 'months').unix(),
    moment().unix(),
];

export function getPeriods(periodStep) {
    const periods = [];

    const from = moment().subtract(3, 'months').unix();
    const to = moment().endOf('day').unix();

    for (let step = from, index = 0; step < to; step += periodStep, index++) {

        periods.push({
            period: step,
            data: getRandomInt(0, 100)
        });
    }

    return {periods};
}

export const StatsCard = ({title, stats, description, data, color}) => (
    <WhiteCardWithTitle title={title} width="30%">
        <Container>
            <Left>
                <Stats>{stats}</Stats>
                <Description>{description}</Description>
            </Left>
            <ChartContainer>
                <LineChart
                    xAxis={false}
                    grid={false}
                    data={data.periods}
                    height={120}
                    stroke={color}
                    xAxisTicks={cardTicks}
                    xAxisDomain={['dataMin', 'dataMax']}
                    xAxisTickFormatter={(tick) => moment(tick * 1000).format('MMM')}
                >
                    <ReferenceLine
                        stroke={color}
                        className='dashed-line'
                        ifOverflow="extendDomain"
                    />
                </LineChart>
            </ChartContainer>
        </Container>
    </WhiteCardWithTitle>
);
