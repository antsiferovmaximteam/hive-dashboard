import moment from "moment";

export function getTicks(periodStep) {
    const from = moment().startOf('day').unix();
    const to = moment().endOf('day').unix();

    const ticks = [];

    for (let step = from + periodStep; step < to; step += periodStep) {
        ticks.push(step);
    }

    return ticks;
}
