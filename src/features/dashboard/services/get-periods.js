import moment from "moment";
import {getRandomInt} from "libs/get-random-int";

export function getPeriods(periodStep) {
    const periods = [];

    const from = moment().startOf('day').unix();
    const to = moment().endOf('day').unix();
    const now = moment().unix();
    let logins = 0;
    let currentPeriod;
    let morningLogis = null;

    for (let step = from, index = 0; step < to; step += periodStep, index++) {
        // logins += index === 0 ? 0 : getRandomInt(0, 100);
        // logins = getRandomInt(10, 30);
        const hour = moment(step * 1000).hour();

        if (hour >= 21 || hour < 6) {
            logins = getRandomInt(1, 3);
        }
        if (hour >= 6 && hour < 10) {
            logins += getRandomInt(-1, 3);
        }
        if (hour >= 10 && hour < 18) {
            if (!morningLogis) {
                morningLogis = logins;
            }

            logins = getRandomInt(
                Math.ceil(morningLogis * .8),
                Math.ceil(morningLogis * 1.2)
            );
        }
        if (hour >= 18 && hour < 21) {
            logins = morningLogis - getRandomInt(1, 2)
        }


        if (step < now) {
            currentPeriod = step;
        }

        periods.push({
            period: step,
            data: step > now ? null : logins
        });
    }

    return data;
}

export function getCurrentPeriods() {
    const now = moment().unix();
    let currentPeriod;
    const res = [];

    for (let item of data.periods) {
        if (item.period <= now) {
            currentPeriod = item.period;
        }

        res.push({
            period: item.period,
            data: item.period > now ? null : item.data
        })
    }

    return {
        currentPeriod,
        periods: res
    }
}

const data = {"currentPeriod":1556848800,"periods":[{"period":1556830800,"data":3},{"period":1556832800,"data":3},{"period":1556834800,"data":3},{"period":1556836800,"data":1},{"period":1556838800,"data":2},{"period":1556840800,"data":2},{"period":1556842800,"data":2},{"period":1556844800,"data":1},{"period":1556846800,"data":2},{"period":1556848800,"data":3},{"period":1556850800,"data":3},{"period":1556852800,"data":5},{"period":1556854800,"data":8},{"period":1556856800,"data":7},{"period":1556858800,"data":9},{"period":1556860800,"data":8},{"period":1556862800,"data":8},{"period":1556864800,"data":10},{"period":1556866800,"data":8},{"period":1556868800,"data":8},{"period":1556870800,"data":10},{"period":1556872800,"data":11},{"period":1556874800,"data":10},{"period":1556876800,"data":11},{"period":1556878800,"data":10},{"period":1556880800,"data":9},{"period":1556882800,"data":8},{"period":1556884800,"data":10},{"period":1556886800,"data":10},{"period":1556888800,"data":8},{"period":1556890800,"data":10},{"period":1556892800,"data":12},{"period":1556894800,"data":11},{"period":1556896800,"data":9},{"period":1556898800,"data":9},{"period":1556900800,"data":9},{"period":1556902800,"data":9},{"period":1556904800,"data":9},{"period":1556906800,"data":2},{"period":1556908800,"data":3},{"period":1556910800,"data":1},{"period":1556912800,"data":1},{"period":1556914800,"data":1},{"period":1556916800,"data":1}]};
