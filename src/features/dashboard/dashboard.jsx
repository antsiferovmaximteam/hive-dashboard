import React, {Component} from 'react';
import styled from 'styled-components';
import ReferenceLine from 'recharts/lib/cartesian/ReferenceLine';
import moment from 'moment';

import {ScreenWithHeader, LineChart, WhiteCardWithTitle} from 'ui';
import {StatsCard} from './ui';
import {getPeriods, getCurrentPeriods} from './services/get-periods';
import {getTicks} from './services/get-ticks';

const Title = styled('h2')`
  font-weight: bold;
  font-size: 26px;
  color: #3E466D;
  letter-spacing: 0;
  line-height: 40px;
`;

const TitleData = styled('span')`
  margin-left: 7px;
  font-weight: bold;
  font-size: 14px;
  color: #504A8E;
  letter-spacing: 0;
  line-height: 40px;
`;

const ChartContainer = styled('div')`
  margin-top: 25px;
  position: relative;
`;

const StatsContainer = styled('div')`
  display: flex;
  justify-content: space-between;
  margin-top: 40px;
`;

const card1Data = {"periods":[{"period":1553856421,"data":989},{"period":1554136421,"data":877},{"period":1554416421,"data":819},{"period":1554696421,"data":911},{"period":1554976421,"data":838},{"period":1555256421,"data":858},{"period":1555536421,"data":996},{"period":1555816421,"data":854},{"period":1556096421,"data":995},{"period":1556376421,"data":936}],"currentPeriod":1556376421};
const card2Data = {"periods":[{"period":1553856543,"data":723},{"period":1554136543,"data":991},{"period":1554416543,"data":934},{"period":1554696543,"data":728},{"period":1554976543,"data":930},{"period":1555256543,"data":954},{"period":1555536543,"data":736},{"period":1555816543,"data":869},{"period":1556096543,"data":772},{"period":1556376543,"data":837}],"currentPeriod":1556376543};
const card3Data = {"periods":[{"period":1553856631,"data":983},{"period":1554136631,"data":945},{"period":1554416631,"data":904},{"period":1554696631,"data":920},{"period":1554976631,"data":984},{"period":1555256631,"data":908},{"period":1555536631,"data":935},{"period":1555816631,"data":926},{"period":1556096631,"data":965},{"period":1556376631,"data":989}],"currentPeriod":1556376631};

export class Dashboard extends Component {
    state = {
        step: 2000,
        periods: null,
        ticks: getTicks(3600),
        currentPeriod: null
    };

    componentDidMount() {
        const data = getCurrentPeriods(this.state.step);

        this.setState({
            periods: data.periods,
            currentPeriod: data.currentPeriod,
        });
    }

    tooltip = payload => (
        `
         <strong>${payload.data}</strong>
         Today, ${moment(payload.period * 1000).format('HH:mm')}
        `
    );

    tickFormatter = (tick) => moment(tick * 1000).format('HH:mm');

    render() {
        return (
            <ScreenWithHeader
                title='Dashboard'
                description='Setup a mobile, web or IoT application to use Hive for Authentication'
                modal={true}
            >
                <WhiteCardWithTitle title='Login Activity'>
                    <Title>Today <TitleData>({moment().format('MMMM D, YYYY')})</TitleData></Title>
                    <ChartContainer>
                        <LineChart
                            data={this.state.periods}
                            xAxisDomain={[moment().startOf('day').unix(), moment().endOf('day').unix()]}
                            xAxisTickFormatter={this.tickFormatter}
                            xAxisTicks={[...this.state.ticks, moment().endOf('day').unix()]}
                            tooltip={this.tooltip}
                            stroke="#6743DF"
                        >
                            <ReferenceLine
                                x={this.state.currentPeriod}
                                stroke="#6743DF"
                                className='dashed-line'
                                ifOverflow="extendDomain"
                            />
                        </LineChart>
                    </ChartContainer>
                </WhiteCardWithTitle>
                <StatsContainer>
                    <StatsCard
                        title="Users"
                        stats='3,458'
                        description="All Time"
                        color="#D86DE3"
                        data={card1Data}
                    />
                    <StatsCard
                        title="Logins"
                        stats='1,106'
                        description="Last 7 days"
                        color="#FBB81B"
                        data={card2Data}
                    />
                    <StatsCard
                        title="New Signups"
                        stats='402'
                        description="Last 7 days"
                        color="#00D38C"
                        data={card3Data}
                    />
                </StatsContainer>
            </ScreenWithHeader>
        )
    }
}
