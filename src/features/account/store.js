import {createActions, handleActions} from'redux-actions';
import {Api} from "api";

export const {createAccount, getAccount} = createActions({
    CREATE_ACCOUNT: async (payload) => {
        const {data} = await Api.createAccount({
            ...payload,
            mode: "TEST"
        });

        console.log(data);
    },
    GET_ACCOUNT: async () => {
        const {data} = await Api.getAccount();

        return data;
    }
});

export const reducer = handleActions({
    [createAccount]: (state) => state,
    [getAccount]: (state, {payload}) => ({...state, ...payload})
}, {});