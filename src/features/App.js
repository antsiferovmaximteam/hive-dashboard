import React, { Component } from 'react';
import styled from 'styled-components';
import {Route} from 'react-router-dom';
import {connect} from 'react-redux';

import {Aside, Header} from 'ui';
import {Dashboard} from 'features/dashboard/dashboard';
import {Applications} from 'features/applications';
import {Logs} from 'features/logs';
import {FraudPrevention} from 'features/fraud-prevention/fraud-prevention';
import {Analytics} from 'features/analytics/analytics';
import {Account} from 'features/account/account';
import {getUser} from 'store/user';
import {logout} from 'features/auth/store';

import {media} from 'ui/theme';
import {routes} from 'routes';

const AppContainer = styled('div')`
  display: flex;
  max-width: 1600px;
  margin: 0 auto;
`;

const AsideContainer = styled('div')`
  min-width: 300px;
  height: 100vh;
  border-right: 1px solid #ECF0FF;
`;

const ContentContainer = styled('section')`
  height: calc(100vh - 47px); // 47px - header height
  overflow-y: scroll;
  padding-top: 24px;
  padding-left: 40px;
  
  ${media.lessThan('huge')`
    padding-right: 40px;
  `}
`;

const HeaderContainer = styled('div')`
  border-bottom: 1px solid #ECF0FF;
`;

const RightColumn = styled('div')`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

class App extends Component {
    componentDidMount() {
        !this.props.user.name && this.props.getUser();
    }

    logout = async () => {
      await this.props.logout();
      this.props.history.push(routes.auth.login.path);
    };

    render() {
        return (
            <AppContainer className="hive-dashboard">
                <AsideContainer>
                    <Aside/>
                </AsideContainer>
                <RightColumn>
                    <HeaderContainer>
                        <Header name={this.props.user.name} avatar={this.props.user.avatar} logout={this.logout}/>
                    </HeaderContainer>
                    <ContentContainer>
                        <Route path={routes.root.applications.path} component={Applications}/>
                        <Route path={routes.root.logs.path} component={Logs}/>
                        <Route path={routes.root.fraudPrevention.path} component={FraudPrevention}/>
                        <Route path={routes.root.analytics.path} component={Analytics}/>
                        <Route path={routes.root.account.path} component={Account}/>
                        <Route exact path={routes.root.path} component={Dashboard}/>
                    </ContentContainer>
                </RightColumn>
            </AppContainer>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user
});

const mapDispatchToProps = dispatch => ({
    getUser: () => dispatch(getUser()),
    logout: () => dispatch(logout())
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
