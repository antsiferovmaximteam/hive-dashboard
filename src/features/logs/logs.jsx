import React, {Component} from 'react';
import styled, {injectGlobal} from 'styled-components';
import moment from 'moment';

import {SelectContainer, Modal, ScreenHeader, MainScreen} from 'ui';
import {TimeSelect, LogsList, ModalRequest} from './ui';

injectGlobal`
  .modal-logs-content {
    width: 100%;
    height: 100%;
    max-width: 30vw;
    max-height: 100vh;
    background: white;
    margin: 0;
  }
  
  .modal-logs-overlay {
    display: flex;
    justify-content: flex-end;
  }
`;

const SortingContainer = styled('div')`
  span {
    font-weight: 600;
    font-size: 12px;
    color: #504A8E;
    letter-spacing: 0;
    text-align: right;
    line-height: 18px;
    margin: 0 8px;
    
    &:first-child {
      margin-left: 0;
    }
  }
`;

const HeaderContainer = styled('div')`
  display: flex;
  flex-direction: column;
`;

export class Logs extends Component {
    state = {
        show: false
    };

    onLogClick = () => {
        this.setState({show: true})
    };
    
    render() {
        return (
            <MainScreen
                header={
                    <HeaderContainer>
                        <ScreenHeader>Logs</ScreenHeader>
                        <SortingContainer>
                            <span>Show logs for</span>
                            <SelectContainer>
                                <select name="applications" id="applications">
                                    <option value="all">All applications</option>
                                </select>
                            </SelectContainer>
                            <span>for the past</span>
                            <TimeSelect/>
                        </SortingContainer>
                    </HeaderContainer>
                }
            >
                <LogsList onClick={this.onLogClick} logs={logs}/>
                <Modal
                    isOpen={this.state.show}
                    onClose={() => this.setState({show: false})}
                    classContent="modal-logs-content"
                    classOverlay="modal-logs-overlay"
                >
                    <ModalRequest request={request}/>
                </Modal>
            </MainScreen>
        );
    }
}

const request = {
    method: 'POST',
    url: '/v1/calculate',
    code: 411,
    date: moment().subtract(1, 'hours').unix(),
    blocks: [
        {
            name: 'General',
            headers: [
                {title: 'Host', value: 'risk.revizo.com'},
                {title: 'Path', value: '/v1/calculate'},
                {title: 'API Version', value: '2017-07-09'},
                {title: 'Request IP', value: '168.546.78.456'},
            ]
        },
        {
            name: 'Request headers',
            headers: [
                {title: 'Version', value: 'HTTP/1.1'},
                {title: 'Host', value: 'risk.revizo.com'},
                {title: 'User-Agent', value: 'curl/7.54.0'},
                {title: 'Accept-Encoding', value: 'gzip'},
            ]
        },
        {
            name: 'Request headers',
            headers: [
                {title: 'Version', value: 'HTTP/1.1'},
                {title: 'Host', value: 'risk.revizo.com'},
                {title: 'User-Agent', value: 'curl/7.54.0'},
                {title: 'Accept-Encoding', value: 'gzip'},
            ]
        },
        {
            name: 'Request headers',
            headers: [
                {title: 'Version', value: 'HTTP/1.1'},
                {title: 'Host', value: 'risk.revizo.com'},
                {title: 'User-Agent', value: 'curl/7.54.0'},
                {title: 'Accept-Encoding', value: 'gzip'},
            ]
        },
        {
            name: 'Request params',
            headers: [
                {title: 'Version', value: 'HTTP/1.1'},
                {title: 'Host', value: 'risk.revizo.com'},
                {title: 'User-Agent', value: 'curl/7.54.0'},
                {title: 'Accept-Encoding', value: 'gzip'},
            ]
        },
        {
            name: 'Request headers',
            headers: [
                {title: 'Version', value: 'HTTP/1.1'},
            ]
        },
        {
            name: 'Response body',
            body: JSON.stringify({status: 400, data: {error: true,message: 'Bad request'}})
        }
    ]
};

const logs = [
    {method: 'post', url: '/v1/calculate', code: 200, date: moment().subtract(1, 'hours').unix()},
    {method: 'get', url: '/v1/calculate', code: 400, date: moment().subtract(1.5, 'hours').unix()},
    {method: 'post', url: '/v1/calculate', code: 200, date: moment().subtract(1, 'hours').unix()},
    {method: 'get', url: '/v1/calculate', code: 400, date: moment().subtract(1.5, 'hours').unix()},
    {method: 'post', url: '/v1/calculate', code: 200, date: moment().subtract(1, 'hours').unix()},
    {method: 'get', url: '/v1/calculate', code: 400, date: moment().subtract(1.5, 'hours').unix()},
    {method: 'post', url: '/v1/calculate', code: 200, date: moment().subtract(1, 'hours').unix()},
    {method: 'get', url: '/v1/calculate', code: 400, date: moment().subtract(1.5, 'hours').unix()},
    {method: 'post', url: '/v1/calculate', code: 200, date: moment().subtract(1, 'hours').unix()},
    {method: 'get', url: '/v1/calculate', code: 400, date: moment().subtract(1.5, 'hours').unix()},
    {method: 'post', url: '/v1/calculate', code: 200, date: moment().subtract(1, 'hours').unix()},
    {method: 'get', url: '/v1/calculate', code: 400, date: moment().subtract(1.5, 'hours').unix()},
    {method: 'post', url: '/v1/calculate', code: 200, date: moment().subtract(1, 'hours').unix()},
    {method: 'get', url: '/v1/calculate', code: 400, date: moment().subtract(1.5, 'hours').unix()},
    {method: 'post', url: '/v1/calculate', code: 200, date: moment().subtract(1, 'hours').unix()},
    {method: 'get', url: '/v1/calculate', code: 400, date: moment().subtract(1.5, 'hours').unix()},
    {method: 'post', url: '/v1/calculate', code: 200, date: moment().subtract(1, 'hours').unix()},
    {method: 'get', url: '/v1/calculate', code: 400, date: moment().subtract(1.5, 'hours').unix()},
    {method: 'post', url: '/v1/calculate', code: 200, date: moment().subtract(1, 'hours').unix()},
    {method: 'get', url: '/v1/calculate', code: 400, date: moment().subtract(1.5, 'hours').unix()},
    {method: 'post', url: '/v1/calculate', code: 200, date: moment().subtract(1, 'hours').unix()},
    {method: 'get', url: '/v1/calculate', code: 400, date: moment().subtract(1.5, 'hours').unix()},
    {method: 'post', url: '/v1/calculate', code: 200, date: moment().subtract(1, 'hours').unix()},
    {method: 'get', url: '/v1/calculate', code: 400, date: moment().subtract(1.5, 'hours').unix()},
    {method: 'post', url: '/v1/calculate', code: 200, date: moment().subtract(1, 'hours').unix()},
    {method: 'get', url: '/v1/calculate', code: 400, date: moment().subtract(1.5, 'hours').unix()},
    {method: 'post', url: '/v1/calculate', code: 200, date: moment().subtract(1, 'hours').unix()},
    {method: 'get', url: '/v1/calculate', code: 400, date: moment().subtract(1.5, 'hours').unix()},
    {method: 'post', url: '/v1/calculate', code: 200, date: moment().subtract(1, 'hours').unix()},
    {method: 'get', url: '/v1/calculate', code: 400, date: moment().subtract(1.5, 'hours').unix()},
    {method: 'post', url: '/v1/calculate', code: 200, date: moment().subtract(1, 'hours').unix()},
    {method: 'get', url: '/v1/calculate', code: 400, date: moment().subtract(1.5, 'hours').unix()},
];
