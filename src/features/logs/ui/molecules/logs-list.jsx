import React from 'react';
import styled from 'styled-components';
import moment from "moment";

import {ResponseCode} from '../atoms';

const LogsListWrapper = styled('ul')`
  display: flex;
  flex-direction: column;
  width: 100%;
  background: #FFFFFF;
  box-shadow: 0 2px 4px 0 rgba(0,0,0,0.06);
  border-radius: 0 0 2px 2px;
  list-style: none;
`;

const LogsItem = styled('li')`
  display: flex;
  justify-content: space-between;
  padding: 13px 24px;
  border-bottom: 1px solid #F4F6FF;
  cursor: pointer;
  
  &:last-child {
    border-bottom-color: transparent;
  }
`;

const Endpoint = styled('div')`
  font-weight: 600;
  font-size: 14px;
  color: #3E466D;
  line-height: 22px;
  min-width: 45%;
  
  strong {
    text-transform: uppercase;
    margin-right: 4px;
  }
`;

const Response = styled('div')`
    font-weight: 400;
    font-size: 12px;
    color: #504A8E;
    margin-right: auto;
`;

const Code = styled(ResponseCode)`
  font-weight: 600;
  font-size: 14px;
  margin-left: 4px;
`;

const Date = styled('div')`
  font-weight: 400;
  font-size: 12px;
  color: #504A8E;
  
  strong {
    font-weight: 600;
    font-size: 14px;
    color: #3E466D;
    margin-left: 4px;
  }
`;

export const LogsList = ({logs, onClick}) => (
    <LogsListWrapper>
        {logs.map((item, index) => (
            <LogsItem key={index} onClick={onClick}>
                <Endpoint><strong>{item.method}</strong>{item.url}</Endpoint>
                <Response>Response: <Code code={item.code}>{item.code}</Code></Response>
                <Date>Date: <strong>{moment.unix(item.date).format('hh:mm a - DD/MM/Y')}</strong></Date>
            </LogsItem>
        ))}
    </LogsListWrapper>
);