import React from 'react';
import styled from 'styled-components';
import moment from 'moment';
import JSONPretty from 'react-json-pretty';

import {ResponseCode} from '../atoms';

const Wrapper = styled('div')`
  display: flex;
  flex-direction: column;
  width: 100%;
  padding: 32px;
`;

const Title = styled('h3')`
  width: 100%;
  font-weight: 600;
  font-size: 18px;
  color: #504A8E;
  line-height: 26px;
  
  strong {
    color: #3E466D;
  }
`;

const Info = styled('div')`
  display: flex;
  justify-content: space-between;
  margin: 8px 0;
`;

const Code = styled(ResponseCode)`
  font-weight: 600;
  font-size: 12px;
  color: #F8535C;
  line-height: 18px;
`;

const Time = styled('span')`
  font-weight: 600;
  font-size: 12px;
  color: #3E466D;
  line-height: 18px;
`;

const Block = styled('div')`
  margin: 15px 0;
  
  h4 {
      padding-bottom: 5px;
      margin-bottom: 8px;
      border-bottom: 1px solid #EDF0FE;
      font-weight: 600;
      font-size: 14px;
      color: #3E466D;
  }
`;

const Headers = styled('ul')`
  list-style: none;
  padding: 0;
  margin: 0;
  
  li {
    display: flex;
    margin: 8px 0;
    font-weight: 400;
    font-size: 12px;
    color: #504A8E;
    line-height: 18px;
  }
`;

const Header = styled('span')`
  display: block;
  min-width: 170px;
`;

const Value = styled('span')`
  font-weight: 600;
`;

export const ModalRequest = ({request}) => (
    <Wrapper>
        <Title><strong>{request.method}</strong> {request.url}</Title>
        <Info>
            <Code code={request.code}>{request.code}</Code>
            <Time>{moment.unix(request.date).format('hh:mm a - DD/MM/Y')}</Time>
        </Info>
        {request.blocks.map((block, blockIndex) => (
            <Block key={block.name + blockIndex}>
                <h4>{block.name}</h4>
                {block.headers && (
                    <HeadersComponent headers={block.headers}/>
                )}
                {block.body && (
                    <BodyComponent body={block.body}/>
                )}
            </Block>
        ))}
    </Wrapper>
);

const HeadersComponent = ({headers}) => (
    <Headers>
        {headers.map(header => (
            <li key={header.value}>
                <Header>{header.title}</Header>
                <Value>{header.value}</Value>
            </li>
        ))}
    </Headers>
);

const theme = {
    main: 'line-height:1.3;color:rgb(119,119,119);background:#F7F8F9;overflow:auto;',
    key: 'color:rgb(103, 117, 224);',
    string: 'color:rgb(33, 148, 118);',
    value: 'color:rgb(258, 119, 99);',
    boolean: 'color:rgb(218,17,21);',
};

const BodyComponent = ({body}) => (
    <JSONPretty id="request-body" data={body} theme={theme}/>
);
