import React, {Component} from 'react';
import styled from "styled-components";
import {DateRangePicker, Button} from 'ui';
import moment from 'moment';

const TimeSelectWrapper = styled('div')`
  position: relative;
  display: inline-block;
  padding: 0 3px;
  background: white;
  border-radius: 20px;
  border: 1px solid #EDF0FE;
`;

const TimeOption = styled('div')`
  display: inline-block;
  color: ${p => p.active ? 'white' : '#95A0D2'};
  transition: all .2s ease-in-out;
  
  button {
    font-weight: 600;
    font-size: 12px;
    color: ${p => p.active ? 'white' : '#95A0D2'};
    transition: all .2s ease-in-out;
  }
  
  & > * {
    position: relative;
    z-index: 10;
  }
`;

const Indicator = styled('div')`
  position: absolute;
  z-index: 5;
  width: 45px;
  height: 22px;
  background: #6743DF;
  border-radius: 30px;
  left: 3px;
  top: 2px;
  transition: all .2s ease-in-out;
`;

export class TimeSelect extends Component {
    state = {
        time: 'weeks',
        value: {
            from: moment().subtract(1, 'weeks').unix(),
            to: moment().unix()
        }
    };

    componentDidMount() {
        this.drawIndicator(this.state.time);
    }

    drawIndicator = (name) => {
        setTimeout(() => {
            const option = this.timeSelect.querySelector(`[data-name='${name}']`);

            this.indicator.style.width = option.clientWidth + 'px';
            this.indicator.style.height = (this.timeSelect.clientHeight - 4) + 'px';
            this.indicator.style.left = option.offsetLeft + 'px';
            this.indicator.style.top = 2 + 'px';
        }, 20);
    };

    onOptionClick = ({currentTarget}) => {
        this.setState({time: currentTarget.dataset.name});
        this.drawIndicator(currentTarget.dataset.name);

        if (currentTarget.dataset.name !== 'range') {
            this.setState({
                value: {
                    from: moment().subtract(1, currentTarget.dataset.name).unix(),
                    to: moment().unix()
                }
            })
        }
    };

    onRangeUpdate = ({startDate, endDate}) => {
        
        this.setState({
            value: {
                from: startDate.unix(),
                to: endDate.unix()
            }
        });
    };

    render() {
        return (
            <TimeSelectWrapper innerRef={el => this.timeSelect = el}>
                <Indicator innerRef={el => this.indicator = el}/>
                <TimeOption
                    data-name='weeks'
                    onClick={this.onOptionClick}
                    active={this.state.time === 'weeks'}
                >
                    <Button>Week</Button>
                </TimeOption>
                <TimeOption
                    data-name='months'
                    onClick={this.onOptionClick}
                    active={this.state.time === 'months'}
                >
                    <Button>Month</Button>
                </TimeOption>
                <TimeOption
                    data-name='years'
                    onClick={this.onOptionClick}
                    active={this.state.time === 'years'}
                >
                    <Button>Year</Button>
                </TimeOption>
                <TimeOption
                    data-name='range'
                    onClick={this.onOptionClick}
                    active={this.state.time === 'range'}
                >
                    <DateRangePicker onUpdate={this.onRangeUpdate}/>
                </TimeOption>
            </TimeSelectWrapper>
        );
    }
}