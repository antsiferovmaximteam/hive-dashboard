import styled from 'styled-components';

export const ResponseCode = styled('strong')`
  color: ${p => {
    switch (true) {
        case p.code >= 200 && p.code < 300: return '#2DDD34';
        case p.code >= 400 && p.code < 500: return '#F8535C';
        default: return '#F8535C';
    }
  }};
`;