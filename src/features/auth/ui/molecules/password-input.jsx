import React, {Component} from 'react';
import styled from 'styled-components';

import {InputWrapper, Label, Input, ShowPasswordIcon} from '../';
import {Button} from 'ui';

const ShowPasswordButton = styled(Button)`
  position: absolute;
  bottom: 7px;
  right: 7px;
`;

export class PasswordInput extends Component {
    state = {
        show: false
    };

    toggle = () => this.setState((state) => ({show: !state.show}));

    render() {
        const {children, name, placeholder, onChange, value} = this.props;

        return (
            <InputWrapper>
                <Label>{children}</Label>
                <Input
                    name={name}
                    id={name}
                    placeholder={placeholder}
                    type={this.state.show ? 'text' : 'password'}
                    onChange={(e) => onChange(e.target.value, name)}
                    value={value}
                />
                <ShowPasswordButton onPointerDown={this.toggle} onPointerUp={this.toggle}>
                    <ShowPasswordIcon show={this.state.show}/>
                </ShowPasswordButton>
            </InputWrapper>
        )
    }
}