import React from 'react';

import {Input, InputWrapper, Label} from '../';

export const TextInput = ({children, name, placeholder, onChange, value}) => (
    <InputWrapper>
        <Label htmlFor={name}>{children}</Label>
        <Input
            name={name}
            id={name}
            placeholder={placeholder}
            onChange={(e) => onChange(e.target.value, name)}
            value={value}
        />
    </InputWrapper>
);