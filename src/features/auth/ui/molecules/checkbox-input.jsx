import React from 'react';
import styled from 'styled-components';

import {InputWrapper, Label, Description} from '../';

const CheckboxItem = styled('li')`
  position: relative;
  margin-right: 10px;
  
  label {
    display: block;
    padding: 8px 15px;
    border: 1px solid #DEE0E7;
    box-shadow: 0 2px 9px 0 rgba(0,0,0,0.03);
    border-radius: 4px;
    cursor: pointer;
    color: #767FAB;
    font-size: 14px;
    letter-spacing: 0;
    line-height: 22px;
    transition: all .2s;
  }
`;

const CheckboxContainer = styled('ul')`
  display: flex;
  padding: 0;
  margin: 5px 0 0;
  list-style: none;
  
  input {
    visibility: hidden;
    position: absolute;
    
    &:checked ~ label {
      color: #7956EF;
      border-color: #7956EF;
    }
  }
`;

export const CheckboxInput = ({children, description, items, type, name}) => (
    <InputWrapper>
        <Label>{children}</Label>
        <Description>{description}</Description>
        <CheckboxContainer>
            {items.map(item => (
                <CheckboxItem key={item.id}>
                    <input type={type} name={name} value={item.value} id={item.id}/>
                    <label htmlFor={item.id}>{item.title}</label>
                </CheckboxItem>
            ))}
        </CheckboxContainer>
    </InputWrapper>
);

CheckboxInput.defaultProps = {
  items: [],
  type: 'checkbox'
};