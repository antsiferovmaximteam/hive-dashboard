import React from 'react';

import {InputWrapper, Label, Select, DropdownIcon, Description} from '../';

export const SelectInput = ({children, name, options, description, onChange, value}) => (
    <InputWrapper>
        <Label htmlFor={name}>{children}</Label>
        {description && <Description>{description}</Description>}
        <Select name={name} id={name} onChange={e => onChange(e.target.value, name)} value={value}>
            {options.map(option => (
                <option key={option.value} value={option.value}>{option.title}</option>
            ))}
        </Select>
        <DropdownIcon/>
    </InputWrapper>
);

SelectInput.defaultProps = {
    options: [],
    onChange: function () {}
};