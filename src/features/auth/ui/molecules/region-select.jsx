import React from 'react';
import styled from 'styled-components';

import {InputWrapper, Label, Description} from '../';

const RegionContainer = styled('ul')`
  display: flex;
  align-items: baseline;
  padding: 0;
  margin: 5px 0 0;
  list-style: none;
`;

const RegionItem = styled('li')`
  position: relative;
  margin-right: -1px;
  
  label {
      display: block;
      padding: 8px 15px;
      border: 1px solid #DEE0E7;
      border-left-color: transparent;
      box-shadow: 0 2px 9px 0 rgba(0,0,0,0.03);
      cursor: pointer;
      color: #767FAB;
      font-size: 14px;
      letter-spacing: 0;
      line-height: 22px;
      transition: all .2s;
      text-transform: uppercase;
      
      span {
        margin-left: 4px;
      }
  }
  
  &:first-child label {
    border-top-left-radius: 4px;
    border-bottom-left-radius: 4px;
    border-left-color: #DEE0E7;
  }
  
  &:last-child label {
    border-top-right-radius: 4px;
    border-bottom-right-radius: 4px;
  }
  
  input {
    position: absolute;
    visibility: hidden;
    
    &:checked ~ label {
      color: #7956EF;
      border-color: #7956EF;
    }
  }
`;

export const RegionSelect = ({children, name, description, items, onChange, value}) => (
    <InputWrapper>
        <Label>{children}</Label>
        {description && <Description>{description}</Description>}
        <RegionContainer>
            {items.map(item => (
                <RegionItem key={item.value}>
                    <input
                        type="radio"
                        name={name}
                        id={item.value}
                        checked={value === item.value}
                        value={item.value}
                        onChange={e => onChange(e.target.value, name)}
                    />
                    <label htmlFor={item.value}>
                        <img src={item.img} alt={item.title}/>
                        <span>{item.title}</span>
                    </label>
                </RegionItem>
            ))}
        </RegionContainer>
    </InputWrapper>
);

RegionSelect.defaultProps = {
    items: []
};