import styled from "styled-components";

export const Label = styled('label')`
  font-weight: 600;
  font-size: 12px;
  color: #3E466D;
  letter-spacing: 0;
  text-align: left;
  line-height: 18px;
`;