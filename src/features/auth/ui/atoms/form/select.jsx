import styled from "styled-components";
import dropdownIcon from "assets/images/dropdown-icon.svg";

export const Select = styled('select')`
  padding: 8px 16px;
  margin-top: 4px;
  border: 1px solid #DEE0E7;
  border-radius: 4px;
  background: transparent;
  outline: none;
  font-weight: 400;
  font-size: 14px;
  color: #767FAB;
  letter-spacing: 0;
  text-align: left;
  line-height: 24px;
  -webkit-appearance: none;
`;

export const DropdownIcon = styled('div')`
  z-index: 100;
  position: absolute;
  display: block;
  width: 9px;
  height: 6px;
  right: 15px;
  bottom: 17px;
  background: url(${dropdownIcon}) no-repeat;
`;