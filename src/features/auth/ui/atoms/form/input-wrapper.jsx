import styled from "styled-components";

export const InputWrapper = styled('div')`
  display: flex;
  flex-direction: column;  
  width: 100%;
  margin: 10px 0;
  position: relative;
`;