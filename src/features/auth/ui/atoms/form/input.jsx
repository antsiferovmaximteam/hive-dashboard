import styled from "styled-components";

export const Input = styled('input')`
  padding: 8px 16px;
  margin-top: 4px;
  border: 1px solid #DEE0E7;
  border-radius: 4px;
  background: transparent;
  outline: none;
  font-weight: 400;
  font-size: 14px;
  color: #31206B;
  letter-spacing: 0;
  text-align: left;
  line-height: 24px;
  
  &::placeholder {
    color: #767FAB;
  }
  
  &:focus {
    box-shadow: 0 0 4px 0 rgba(103,67,223,0.44);
  }
`;