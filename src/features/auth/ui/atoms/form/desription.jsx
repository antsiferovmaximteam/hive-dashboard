import styled from 'styled-components';

export const Description = styled('p')`
  padding: 0;
  margin: 0;
  font-size: 12px;
  color: #767FAB;
  letter-spacing: 0;
  text-align: left;
  line-height: 18px;
`;