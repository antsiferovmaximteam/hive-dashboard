import styled from 'styled-components';

export const Form = styled('div')`
  display: flex;
  flex-direction: column;
  width: 100%;
  max-width: 377px;
  margin-top: 20px;
  margin-bottom: 35px;
`;