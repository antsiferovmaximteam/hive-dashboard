import React from 'react';
import styled from 'styled-components';

const SVG = styled('svg')`
  width: 20px;
  height: 20px;
  margin-right: 8px;
`;

export const ButtonIcon = ({children}) => (
    <SVG xmlns="http://www.w3.org/2000/svg">
        {children}
    </SVG>
);