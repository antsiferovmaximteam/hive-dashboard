import styled from 'styled-components';

export const Or = styled('div')`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: space-between; 
  width: 150px;
  height: 18px;
  text-transform: uppercase;
  font-size: 12px;
  color: #767FAB;
  
  &:after, &:before {
    content: '';
    display: block;
    height: 1px;
    background: #EBEBEB;
    width: 37%;
  }
`;