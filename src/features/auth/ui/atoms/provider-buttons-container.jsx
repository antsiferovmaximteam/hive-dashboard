import styled from 'styled-components';

export const ProviderButtonsContainer = styled('div')`
  display: flex;
  margin-top: 24px;
  margin-bottom: 24px;
  
  & > * {
    margin: 0 8px;
  }
`;