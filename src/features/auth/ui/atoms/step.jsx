import styled from 'styled-components';

export const Step = styled('p')`
  margin: 0;
  font-weight: 600;
  font-size: 12px;
  color: #767FAB;
  text-align: center;
  line-height: 18px;
  text-transform: uppercase;
`;