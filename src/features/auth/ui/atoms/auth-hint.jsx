import React from 'react';
import styled from 'styled-components';
import {Link as RouteLink} from "react-router-dom";

const Link = styled(RouteLink)`
  font-weight: 400;
  font-size: 14px;
  color: #6515DD;
  letter-spacing: 0;
  line-height: 18px;
  
  &:hover {
    color: #6515DD;
  }
`;

const Text = styled('p')`
  margin-top: 25px;
  font-size: 14px;
  color: #3E466D;
  text-align: left;
  line-height: 22px;
`;

export const AuthHint = ({children, to, linkText}) => (
    <Text>{children} <Link to={to}>{linkText}</Link></Text>
);
