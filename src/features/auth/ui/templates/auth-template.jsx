import React from 'react';
import styled from 'styled-components';

const Wrapper = styled('div')`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: 100%;
`;

const Title = styled('h3')`
  font-weight: bold;
  font-size: 22px;
  color: #6743DF;
  text-align: center;
`;

const Subtitle = styled('p')`
  font-size: 14px;
  color: #767FAB;
  text-align: center;
  max-width: 330px;
`;

export const AuthTemplate = ({children, title, subtitle}) => (
    <Wrapper>
        <Title>{title}</Title>
        <Subtitle>{subtitle}</Subtitle>
        {children}
    </Wrapper>
);