import React from 'react';
import {Route} from 'react-router-dom';
import styled from 'styled-components';

import logo from 'assets/images/logo.svg';
import authBackground from './assets/auth-background.png';
import adrenalin from './assets/adrenalin.png';
import author from './assets/author.png';
import Login from './features/login/login';
import Registration from './features/registration/registration';
import Account from './features/account/account';
import {routes} from "routes";

const css = String.raw;

const Wrapper = styled('div')`
  display: flex;
  width: 100vw;
  height: 100vh;
`;

const Right = styled('div')`
  width: 50%;
  height: 100%; 
`;

const AuthHero = styled('div')`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  position: relative;
  height: 100%;
  width: 50%;
  background: url("${authBackground}") no-repeat center center / cover;
  color: white;
  text-align: center;
`;

const Logo = styled('div')`
  position: absolute;
  right: 0;
  top: 50%;
  height: 102px;
  width: 102px;
  display: flex;
  align-items: center;
  justify-content: center;
  transform: translate(50%, -50%);
  border-radius: 50%;
  background: white;
  box-shadow: 0 19px 33px 0 rgba(50,50,93,0.06), 0 0 14px 0 rgba(0,0,0,0.07);
  
  img {
    width: 50px;
  }
`;

const AuthTitle = styled('h2')`
  font-weight: 900;
  font-size: 22px;
`;

const AuthDescription = styled('p')`
  font-weight: 400;
  font-size: 14px;
`;

const NavDotsList = styled('ul')`
  display: flex;
  align-items: center;
  margin-top: 48px;
  list-style: none;
`;

const NavDotsItem = styled('li')`
  width: 8px;
  height: 8px;
  margin: 0 5px;
  border-radius: 50%;
  background: rgba(255, 255, 255);
  opacity: .8;
  cursor: pointer;
  
  ${p => p.active && css`
    height: 12px;
    width: 12px;
    opacity: 1;
  `}
`;

const Content = styled('div')`
  display: flex;
  flex-direction: column;
  max-width: 473px;
`;

const AdrenalinLogo = styled('img')`
  width: 100%;
  max-width: 98px;
`;

const Quote = styled('p')`
  font-size: 24px;
  line-height: 29px;
  text-align: left;
`;

const Author = styled('div')`
  display: flex;
  align-items: center;
`;

const Avatar = styled('img')`
  width: 45px;
  height: 45px;
`;

const Info = styled('div')`
  margin-left: 11px;
  font-size: 14px;
  line-height: 17px;
`;

const Name = styled('p')`
  font-weight: 700;
  text-align: left;
  margin: 0;
`;

const Position = styled('p')`
  font-weight: 400;
  margin: 0;
`;

const Auth = () => (
    <Wrapper>
        <AuthHero>
            <Content>
                <AdrenalinLogo src={adrenalin} alt="adrenalin"/>
                <Quote>“With Hive.id we can focus on the core of our product and minimize analyst hours on manual review.”</Quote>
                <Author>
                    <Avatar src={author} alt="Author"/>
                    <Info>
                        <Name>Alexey Galtsov</Name>
                        <Position>Co-founder & CEO</Position>
                    </Info>
                </Author>
            </Content>
            <Logo><img src={logo} alt=""/></Logo>
        </AuthHero>
        <Right>
            <Route path={routes.auth.login.path} component={Login}/>
            <Route path={routes.auth.registration.path} component={Registration}/>
            <Route path={routes.auth.account.path} component={Account}/>
        </Right>
    </Wrapper>
);

export default Auth;
