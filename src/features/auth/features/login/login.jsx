import React, {Component} from 'react';
import styled from 'styled-components';
import {Link} from 'react-router-dom';
import { connect } from 'react-redux';

import {getToken, clearErrorMsg, loginWithGoogle, refreshToken} from '../../store';
import {routes} from "routes";
import {GoogleAuth} from 'libs/google-auth';
import {
    AuthTemplate,
    HiveButton,
    GoogleButton,
    Or,
    AuthButton,
    TextInput,
    PasswordInput,
    ProviderButtonsContainer,
    Form,
    AuthHint
} from '../../ui';

const ForgotLink = styled(Link)`
  font-weight: 400;
  font-size: 12px;
  color: #7956EF;
  letter-spacing: 0;
  line-height: 18px;
  margin-top: -5px;
  
  &:hover {
    color: #7956EF;
  }
`;

export class Login extends Component {
    state = {
        data: {
            email: '',
            password: ''
        }
    };

    handleInput = (value, name) => {
        this.props.errorMsg && this.props.clearErrorMsg();

        this.setState(state => ({
            data: {
                ...state.data,
                [name]: value
            }
        }))
    };

    handleLogin = async () => {
        this.props.getToken(
            this.state.data.email,
            this.state.data.password
        );
    };

    onGoogleClick = async () => {
        const googleToken = await GoogleAuth.authorize();
        await this.props.loginWithGoogle(googleToken);
        await this.props.refreshToken();
    };

    componentDidUpdate() {
        console.log(this.props.access_token);
        this.props.access_token && this.props.history.push(routes.root.path);
    }

    render() {
        return (
            <AuthTemplate
                title="Welcome to Hive!"
                subtitle="Help us setup your first tenant and start autentification tyrtrytrytwewtu"
            >
                <ProviderButtonsContainer>
                    <GoogleButton onClick={this.onGoogleClick}>Log In with Google</GoogleButton>
                    <HiveButton>Log In with Hive</HiveButton>
                </ProviderButtonsContainer>
                <Or>Or</Or>
                <Form>
                    <TextInput
                        name="email"
                        onChange={this.handleInput}
                        value={this.state.data.email}
                        placeholder="email@company.com"
                    >Email</TextInput>
                    <PasswordInput
                        name="password"
                        onChange={this.handleInput}
                        value={this.state.data.password}
                        placeholder="Enter your password"
                    >Password</PasswordInput>
                    <ForgotLink to="/auth/forgot">Forgot your password?</ForgotLink>
                </Form>
                <AuthButton onClick={this.handleLogin}>Log in</AuthButton>
                <AuthHint
                    linkText="Sign Up"
                    to="/auth/registration"
                >
                    Don’t have an account ?
                </AuthHint>
            </AuthTemplate>
        );
    }
}

const mapStateToProps = state => ({
    errorMsg: state.auth.errorMsg,
    access_token: state.auth.access_token,
    refresh_token: state.auth.refresh_token
});

const mapDispatchToProps = (dispatch, props) => ({
    getToken: (email, password) => dispatch(getToken(email, password)),
    loginWithGoogle: (token) => dispatch(loginWithGoogle(token)),
    clearErrorMsg: () => dispatch(clearErrorMsg()),
    refreshToken: () => dispatch(refreshToken(props.refresh_token))
});

const LoginConnected = connect(mapStateToProps, mapDispatchToProps)(Login);

export default LoginConnected;