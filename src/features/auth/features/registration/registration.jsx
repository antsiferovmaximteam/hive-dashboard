import React, {Component} from 'react';
import { connect } from 'react-redux';
import {routes} from 'routes';
import {GoogleAuth} from 'libs/google-auth';

import {
    AuthTemplate,
    HiveButton,
    GoogleButton,
    ProviderButtonsContainer,
    Or,
    Form,
    TextInput,
    PasswordInput,
    AuthButton,
    AuthHint,
    Step
} from '../../ui';
import {
    getToken,
    clearErrorMsg,
    refreshToken,
    registration,
    registrationWithGoogle
} from '../../store';

export class Registration extends Component {
    state = {
        data: {
            name: '',
            email: '',
            password: ''
        }
    };

    handleInput = (value, name) => {
        this.setState((state) => ({
            data: {
                ...state.data,
                [name]: value
            }
        }))
    };

    handleRegistration = async () => {
        await this.props.registration(
            this.state.data.name,
            this.state.data.email,
            this.state.data.password,
        );

        await this.props.token(
            this.state.data.email,
            this.state.data.password
        );

        this.props.history.push(routes.auth.account.path);
    };

    handleGoogleClick = async () => {
        const googleToken = await GoogleAuth.authorize();
        await this.props.registrationWithGoogle(googleToken);
        await this.props.refreshToken();
        await this.props.history.push(routes.auth.account.path);
    };

    render() {
        return (
            <AuthTemplate
                title="Welcome to Hive"
                subtitle="Help us setup your first tenant and start autentification tyrtrytrytwewtu"
            >
                <Step>Step 1 of 2</Step>
                <ProviderButtonsContainer>
                    <GoogleButton onClick={this.handleGoogleClick}>Sign up with Google</GoogleButton>
                    <HiveButton>Sign up with Hive</HiveButton>
                </ProviderButtonsContainer>
                <Or>Or</Or>
                <Form>
                    <TextInput
                        name="name"
                        onChange={this.handleInput}
                        value={this.state.data.name}
                        placeholder="Enter your First and Last name"
                    >
                        First and Last name
                    </TextInput>
                    <TextInput
                        name="email"
                        onChange={this.handleInput}
                        value={this.state.data.email}
                        placeholder="your@email.com"
                    >
                        Email
                    </TextInput>
                    <PasswordInput
                        name="password"
                        onChange={this.handleInput}
                        value={this.state.data.password}
                        placeholder="Enter your password"
                    >
                        Password
                    </PasswordInput>
                </Form>
                <AuthButton onClick={this.handleRegistration}>Sign up</AuthButton>
                <AuthHint
                    to="/auth/login"
                    linkText="Log In!"
                >
                    Already have an account?
                </AuthHint>
            </AuthTemplate>
        );
    }
}

const mapStateToProps = state => ({
    errorMsg: state.auth.errorMsg,
    access_token: state.auth.access_token
});

const mapDispatchToProps = dispatch => ({
    token: (email, password) => dispatch(getToken(email, password)),
    clearErrorMsg: () => dispatch(clearErrorMsg()),
    registration: (name, email, password) => dispatch(registration(name, email, password)),
    refreshToken: () => dispatch(refreshToken()),
    registrationWithGoogle: (token) => dispatch(registrationWithGoogle(token))
});

const RegistrationConnected = connect(mapStateToProps, mapDispatchToProps)(Registration);

export default RegistrationConnected;