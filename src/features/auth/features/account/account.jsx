import React, {Component} from 'react';
import { connect } from 'react-redux';

import {
    AuthTemplate,
    Step,
    Form,
    TextInput,
    SelectInput,
    // CheckboxInput,
    RegionSelect,
    AuthButton,
    AuthHint
} from '../../ui';

import australia from './assets/australia.png';
import europe from './assets/europe.png';
import usa from './assets/usa.png';
import {createAccount} from "features/account/store";
import {refreshToken} from "features/auth/store";
import {routes} from "routes";

export class Account extends Component {
    state = {
        data: {
            company_name: '',
            employees_num: 'default',
            region: 'default',
            mode: 'TEST'
        }
    };

    handleInput = (value, name) => {
        this.setState((state) => ({
            data: {
                ...state.data,
                [name]: value
            }
        }))
    };

    handleCreateAccount = async () => {
        await this.props.createAccount(this.state.data);
        await this.props.refreshToken();
        this.props.history.push(routes.root.path);
    };

    render() {
        return (
            <AuthTemplate
                title="Tell us more"
                subtitle="These questions will help us better tailor your Revizo expiriense to fit your needs"
            >
                <Step>Step 2 of 2</Step>
                <Form>
                    <TextInput
                        name="company_name"
                        onChange={this.handleInput}
                        value={this.state.data.company_name}
                        placeholder="Apple Incorporation"
                    >
                        Company Name
                    </TextInput>
                    <SelectInput
                        name="employees_num"
                        onChange={this.handleInput}
                        value={this.state.data.employees_num}
                        options={employees}
                    >
                        Employees
                    </SelectInput>
                    {/*<CheckboxInput
                        description="Are you a technical or non technical person?"
                        items={checkboxes}
                        type="radio"
                        name="role"
                    >
                        Role
                    </CheckboxInput>*/}
                    <SelectInput
                        options={project}
                        description="What kind of project are you going to build?"
                    >
                        Project
                    </SelectInput>
                    <RegionSelect
                        name="region"
                        onChange={this.handleInput}
                        value={this.state.data.region}
                        items={regions}
                        description="What kind of project are you going to build?"
                    >
                        Region
                    </RegionSelect>
                </Form>
                <AuthButton onClick={this.handleCreateAccount}>Create account</AuthButton>
                <AuthHint
                    linkText="Terms"
                    to="/auth/terms"
                >
                    By creating an account, you agree to our
                </AuthHint>
            </AuthTemplate>
        );
    }
}

const employees = [
    {
        value: 'default',
        title: 'Select an option'
    },
    {
        value: 'LESS_THAN_10',
        title: 'Less than 10'
    },
    {
        value: 'FROM_10_TO_50',
        title: 'From 10 to 50'
    },
    {
        value: 'FROM_50_TO_200',
        title: 'From 50 to 200'
    },
    {
        value: 'FROM_200_TO_500',
        title: 'From 200 to 500'
    },
    {
        value: 'MORE_THAN_500',
        title: 'More than 500'
    }
];

const project = [
    {
        value: 'default',
        title: 'Select an option'
    },
    {
        value: 'FOR_CONSUMERS',
        title: 'For consumers'
    },
    {
        value: 'FOR_COMPANIES',
        title: 'For companies'
    },
    {
        value: 'FOR_EMPLOYEES',
        title: 'For employees'
    }
];

/*const checkboxes = [
    {
        value: 'dev',
        title: 'Developer',
        id: 'dev'
    },
    {
        value: 'non',
        title: 'Non developer',
        id: 'non'
    },
];*/

const regions = [
    {
        value: 'AU',
        title: 'Au',
        img: australia
    },
    {
        value: 'EU',
        title: 'Eu',
        img: europe
    },
    {
        value: 'US',
        title: 'Us',
        img: usa
    }
];

const mapDispatchToProps = dispatch => ({
    createAccount: (payload) => dispatch(createAccount(payload)),
    refreshToken: () => dispatch(refreshToken())
});

const AccountConnected = connect(null, mapDispatchToProps)(Account);

export default AccountConnected;