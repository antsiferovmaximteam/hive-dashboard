import {createActions, handleActions, combineActions} from 'redux-actions';
import {stateToLocalStorage} from 'libs/state-to-local-storage';
import {secondsToMilliseconds} from 'libs/time';
import {Api} from 'api';

const keysToSave = ['access_token', 'refresh_token', 'expires_in'];
const LOCALSTORAGE_AUTH_KEY = 'auth';
const initialState = JSON.parse(localStorage.getItem(LOCALSTORAGE_AUTH_KEY)) || {};

export const {
    getToken,
    clearErrorMsg,
    registration,
    refreshToken,
    registrationWithGoogle,
    loginWithGoogle,
    logout
} = createActions({
    GET_TOKEN: async (email, password) => {
        if (email.length === 0) {
            return {
                errorMsg: 'You did not enter the email'
            }
        }

        const res = await Api.getToken(email, password);

        const {status, data} = res;

        if (status === 200) {
            return data
        } else {
            return {
                errorMsg: 'Server error'
            }
        }
    },
    REFRESH_TOKEN: async () => {
        let {refresh_token} = JSON.parse(localStorage.getItem('auth'));

        const {data} = await Api.refreshToken(refresh_token);

        return data;
    },
    REGISTRATION: async (name, email, password) => {
        //TODO validation registration params

        await Api.registration(name, email, password);

        //TODO catch errors

        return {};
    },
    REGISTRATION_WITH_GOOGLE: async (token) => {
        const {status, data} = await Api.registrationWithGoogle(token);

        return data
    },
    LOGIN_WITH_GOOGLE: async (token) => {
        const {data} = await Api.loginWithGoogle(token);

        return data;
    },
    LOGOUT: async () => {
        const res = await Api.logout();

        if (!res) {
            return;
        }

        res.status === 204 && localStorage.removeItem(LOCALSTORAGE_AUTH_KEY);
    }
}, 'CLEAR_ERROR_MSG');

export const reducer = handleActions({
    [combineActions(
        getToken,
        registrationWithGoogle,
        loginWithGoogle,
        refreshToken)]: (state, {payload}) => stateToLocalStorage(LOCALSTORAGE_AUTH_KEY, {
        ...state,
        ...payload,
        expires_in: Date.now() + secondsToMilliseconds(payload.expires_in)
    }, keysToSave),
    [clearErrorMsg]: (state) => ({...state, errorMsg: null}),
    [registration]: (state) => state
}, initialState);
