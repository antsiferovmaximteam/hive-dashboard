import React from 'react';
import {Route} from 'react-router-dom';
import {connect} from 'react-redux';

import ApplicationSettings from "./features/application-settings/application-settings";
import {ApplicationScopes} from './features/application-scopes';
import {Breadcrumbs, MainScreen} from 'ui';
import {routes} from "routes";

const mapStateToProps = state => ({
    getAppById: (id) => state.apps.filter(app => app.app_id === id)[0]
});

export const Application = connect(mapStateToProps)(({getAppById, match}) => (
    <MainScreen
        header={
            <Breadcrumbs>
                <Breadcrumbs.Item path={routes.root.applications.path}>Applications</Breadcrumbs.Item>
                <Breadcrumbs.Item>{getAppById(match.params.id) && getAppById(match.params.id).name}</Breadcrumbs.Item>
            </Breadcrumbs>
        }
    >
        <Route
            path={routes.root.applications.application.settings.path}
            component={ApplicationSettings}/>
        <Route
            path={routes.root.applications.application.scopes.path}
            component={ApplicationScopes}/>
    </MainScreen>
));
