import React from 'react';
import styled from 'styled-components';

import check from '../assets/check.svg';
import cancel from '../assets/cancel.svg';

const Wrapper = styled('div')`
  display: flex;
  flex-direction: column;
`;

const Title = styled('h3')`
  font-size: 12px;
  line-height: 18px;
  color: #414042;
  font-weight: 600;
  margin: 4px 0;
`;

const List = styled('ul')`
  list-style: none;
`;

const ListItem = styled('li')`
  display: flex;
  align-items: center;
  font-size: 12px;
  line-height: 18px;
  color: #676668;
  
  &:before {
    content: '';
    display: block;
    margin-right: 8px;
  }
`;

const ListItemCheck = styled(ListItem)`
  &:before {
    width: 12px;
    height: 9px;
    background: url("${check}") no-repeat center center / contain;
  }
`;

const ListItemCancel = styled(ListItem)`
  &:before {
    width: 12px;
    height: 12px;
    background: url("${cancel}") no-repeat center center / contain;
  }
`;

export const TooltipList = ({allowed, disallowed}) => (
    <Wrapper>
        <Title>Allowed Usage</Title>
        <List>
            {allowed.map((item, index) => (
                <ListItemCheck key={index}>{item}</ListItemCheck>
            ))}
        </List>
        <Title>Disallowed Usage</Title>
        <List>
            {disallowed.map((item, index) => (
                <ListItemCancel key={index}>{item}</ListItemCancel>
            ))}
        </List>
    </Wrapper>
);
