import React from 'react';
import styled from 'styled-components';
import {Checkbox} from '../../../atoms';
import {Tooltip} from './tooltip';

const Container = styled('div')`
  display: flex;
  align-items: flex-start;
`;

const CheckboxContainer = styled('div')`
  margin-top: 3px;
`;

const LabelContainer = styled('div')`
  display: flex;
  flex-direction: column;
  margin-left: 8px;
`;

const Label = styled('div')`
  display: flex;
  align-items: center;
`;

const LabelTitle = styled('label')`
  display: flex;
  font-weight: 600;
  user-select: none;
  font-size: 14px;
`;

const LabelCode = styled('code')`
  margin-left: 8px;
  padding: 2px 8px;
  border-radius: 4px;
  background-color: #F1F2F2;
  color: #000000;
  font-size: 14px;
  font-weight: 300;
`;

const Description = styled('p')`
  padding: 0;
  margin: 10px 0 0;
  color: #676668;
  font-size: 12px;
`;

export const CheckboxField = ({name, id = name, value, label, onChange, slug, description, tooltip}) => (
    <Container>
        <CheckboxContainer>
            <Checkbox name={name} id={id} value={value} onChange={onChange}/>
        </CheckboxContainer>
        <LabelContainer>
            <Label>
                <LabelTitle htmlFor={id}>{label}</LabelTitle>
                <LabelCode>{slug}</LabelCode>
                <Tooltip>
                    {tooltip}
                </Tooltip>
            </Label>
            <Description>
                {description}
            </Description>
        </LabelContainer>
    </Container>
);
