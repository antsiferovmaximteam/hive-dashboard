import React, {useState, useRef} from 'react';
import styled from 'styled-components';

import {QuestionIcon} from '../atoms/question.jsx';

const TooltipWrapper = styled('div')`
  margin-left: 8px;
  cursor: pointer;
`;

const TooltipContainer = styled('div')`
  position: fixed;
  padding: 8px;
  display: none;
  opacity: 0;
  transform: translateY(-50%);
  background: #FFFFFF;
  box-shadow: 0 5px 15px 0 rgba(0,0,0,0.08), 0 15px 35px 0 rgba(49,49,93,0.10), 0 0 0 1px rgba(136,152,170,0.10);
  border-radius: 4px;
  transition: opacity .3s ease-in-out;
  
  &:before {
    content: '';
    position: absolute;
    right: 99%;
    top: calc(50% - 10px);
    display: block;
    height: 20px;
    width: 20px;
    background-color: white;
    box-shadow: 0 5px 15px 0 rgba(0,0,0,0.08), 0 15px 35px 0 rgba(49,49,93,0.10), 0 0 0 1px rgba(136,152,170,0.10);
    z-index: 100;
    clip-path: polygon(100% 0, 0 50%, 100% 100%);
  }
`;

export const Tooltip = ({children}) => {
    const icon = useRef(null);
    const container = useRef(null);

    const showTooltip = () => {
        container.current.style.display = 'block';
        const iconBounding = icon.current.getBoundingClientRect();
        container.current.style.left = (iconBounding.left + iconBounding.width + 20) + 'px';
        container.current.style.top = (iconBounding.top + iconBounding.height / 2) + 'px';
        container.current.style.opacity = 1;
    };

    const hideTooltip = () => {
        container.current.style.opacity = 0;
        setTimeout(() => {
            container.current.style.display = 'none';
        }, 300)
    };

    return (
        <TooltipWrapper
            onMouseEnter={showTooltip}
            onMouseLeave={hideTooltip}
            innerRef={icon}
        >
            <QuestionIcon/>
            <TooltipContainer innerRef={container}>
                {children}
            </TooltipContainer>
        </TooltipWrapper>
    )
};
