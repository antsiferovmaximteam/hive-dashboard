import React from 'react';

export const QuestionIcon = ({}) => (
    <svg width="16" height="16" xmlns="http://www.w3.org/2000/svg">
        <g transform="translate(1 1)" fill="none" fillRule="evenodd">
            <circle stroke="#676668" cx="7" cy="7" r="7"/>
            <text fontFamily="Lato-Semibold, Lato" fontSize="10" fontWeight="500" fill="#676668">
                <tspan x="5" y="12">?</tspan>
            </text>
        </g>
    </svg>
);
