import React, {Fragment} from 'react';
import styled from 'styled-components';

import {WhiteCardWithTitle} from 'ui';
import {CheckboxField, TooltipList} from './molecules';
import {SaveButton} from '../application-settings/ui';

const ScopeSaveButton = styled(SaveButton)`
  margin-left: 0;
`;

const Description = styled('p')`
  margin: 22px 0 0;
  font-size: 14px;
`;

const Link = styled('a')`
  color: #6515DD
`;

const BlockTitle = styled('p')`
  font-size: 14px;
  color: #414042;
  margin-top: 28px;
  margin-bottom: 24px;
  
  strong {
    font-size: 16px;
    color: #414042;
    font-weight: bold;
  }
  
  a {
    color: #6515DD;
  }
`;

const InputField = styled('div')`
  margin-top: 16px;
`;

const Code = styled('code')`
  padding: 2px 8px;
  border-radius: 4px;
  background-color: #F1F2F2;
  color: #000000;
  font-weight: 300;
`;

export class ApplicationScopes extends React.Component {
    state = {
        data: {
            email_address: true,
            phone_number: true,
            user_age_range: false,
            user_first_name: false,
            user_last_name: false,
            user_birthday: false,
            gov_id_photo: false,
        }
    };

    handleChange = (value, name) => {
        const data = {
            ...this.state.data,
            [name]: value,
        };

        this.setState({data});
    };

    render() {
        return (
            <WhiteCardWithTitle
                title="Scopes"
                borderedTitle={true}
            >
                <Description>
                    Each scope has its own set of requirements and usage that are
                    subject to our <Link href="#">Platform Policies</Link> and your own privacy policy.
                </Description>
                <BlockTitle><strong>Basic</strong> - do not require <a href="#">App Review</a>.</BlockTitle>
                <InputField>
                    <CheckboxField
                        name="email_address"
                        onChange={this.handleChange}
                        value={this.state.data.email_address}
                        slug="email_address"
                        label="Email Address"
                        description="Grants your app permission to access a person's primary email address and verified email flag."
                        tooltip={
                            <TooltipList
                                allowed={[
                                    'Allow a person to use their email address to login to your app.',
                                    'Confirming bookings, purchases, orders, etc.',
                                    'Sending customer support messages.'
                                ]}
                                disallowed={[
                                    <Fragment>
                                        Spamming users. Your use of email must comply with both&nbsp;<Link href="#">Hive.id</Link>&nbsp;policies and the&nbsp;<Link href="#"> CAN-SPAM Act</Link>.
                                    </Fragment>
                                ]}
                            />
                        }
                    />
                </InputField>
                <InputField>
                    <CheckboxField
                        name="phone_number"
                        onChange={this.handleChange}
                        value={this.state.data.phone_number}
                        slug="phone_number"
                        label="Phone Number"
                        description="Grants your app permission to access a person's primary phone number and verified phone flag."
                        tooltip={
                            <TooltipList
                                allowed={[
                                    'Creating user-initiated interactive experiences',
                                    'Confirming bookings, purchases, orders, etc.',
                                    'Sending customer support messages.'
                                ]}
                                disallowed={[
                                    'Sending brand advertising, newsletters, or spam.',
                                    'Messaging people without their consent.',
                                ]}
                            />
                        }
                    />
                </InputField>
                <InputField>
                    <CheckboxField
                        name="user_age_range"
                        onChange={this.handleChange}
                        value={this.state.data.user_age_range}
                        slug="user_age_range"
                        label="Age Range"
                        description="Grants your app permission to access a person's age range."
                        tooltip={
                            <TooltipList
                                allowed={[
                                    'Your app includes age-gated content, e.g. gambling, gaming, or alcohol.',
                                    'Your app includes content not suitable for the general audience, e.g. dating, mature, or graphic content.',
                                    'Your app includes content that is directed at kids or teens.'
                                ]}
                                disallowed={[
                                    'No visible impact to the User experience based on a person\'s age range.'
                                ]}
                            />
                        }
                    />
                </InputField>
                <BlockTitle><strong>Extended</strong> - require <a href="#">App Review</a>.</BlockTitle>
                <InputField>
                    <CheckboxField
                        name="user_first_name"
                        onChange={this.handleChange}
                        value={this.state.data.user_first_name}
                        slug="user_first_name"
                        label="First Name"
                        description="Grants your app permission to access a person's primary email address."
                        tooltip={
                            <TooltipList
                                allowed={[
                                    'Personalize a person’s experience.',
                                ]}
                                disallowed={[
                                    'Non-visible use of this data.'
                                ]}
                            />
                        }
                    />
                </InputField>
                <InputField>
                    <CheckboxField
                        name="user_last_name"
                        onChange={this.handleChange}
                        value={this.state.data.user_last_name}
                        slug="user_last_name"
                        label="Last Name"
                        description="Grants your app permission to access a person's primary email address."
                        tooltip={
                            <TooltipList
                                allowed={[
                                    'Personalize a person’s experience.',
                                ]}
                                disallowed={[
                                    'Non-visible use of this data.'
                                ]}
                            />
                        }
                    />
                </InputField>
                <InputField>
                    <CheckboxField
                        name="user_birthday"
                        onChange={this.handleChange}
                        value={this.state.data.user_birthday}
                        slug="user_birthday"
                        label="Birthday"
                        description="Grants your app permission to access a person's birthday."
                        tooltip={
                            <TooltipList
                                allowed={[
                                    'Provide age relevant content to people based on their date of birth.',
                                    'Provide age relevant content for anything where the age range is not sufficient.',
                                ]}
                                disallowed={[
                                    <Fragment>Determine whether a person says they are under 18, over 18, or over 21. Please use the&nbsp;<Code>user_age_range</Code>&nbsp;instead.</Fragment>
                                ]}
                            />
                        }
                    />
                </InputField>
                <InputField>
                    <CheckboxField
                        name="gov_id_photo"
                        onChange={this.handleChange}
                        value={this.state.data.gov_id_photo}
                        slug="gov_id_photo"
                        label="Government ID photo"
                        description="Grants your app permission to access a person's primary email address."
                        tooltip={
                            <TooltipList
                                allowed={[
                                    'Required for certain types of businesses for KYC & AML procedures. Please do not use otherwise.',
                                ]}
                                disallowed={[
                                    'Non-visible use of this data.',
                                    'Transfer or sale of this data data to third parties.'
                                ]}
                            />
                        }
                    />
                </InputField>
                <ScopeSaveButton>Save</ScopeSaveButton>
            </WhiteCardWithTitle>
        )
    }
}
