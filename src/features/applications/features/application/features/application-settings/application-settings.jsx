import React, {Component, Fragment} from 'react';
import styled from 'styled-components';
import {connect} from 'react-redux';

import {WhiteCardWithTitle} from 'ui';
import {TextInputFiled, CheckboxField, CopyInputFiled, FileInputFiled, TextareaInputFiled, Phone, SaveButton} from './ui';
import defaultImg from 'assets/images/temp-default-icon.svg';
import {getApplications, updateApplication} from 'features/applications/store';

const Wrapper = styled('div')`
  padding-bottom: 86px;
`;

const Card = styled(WhiteCardWithTitle)`
  margin-top: 24px;
  
  &:first-child {
    margin-top: 0;
  }
  
  h3 {
    border-bottom: 1px solid #F4F6FF;
  }
`;

const CardContainer = styled('div')`
  display: flex;
`;

const CardInputs = styled('div')`
      width: 100%;
`;

const CardPhone = styled('div')`
  max-width: 280px;
  width: 100%;
  display: flex;
  justify-content: center;
  padding-top: 40px;
  margin-bottom: 40px;
`;

const Code = styled('code')`
  font-family: monospace;
  background-color: rgb(245,247,249);
  padding: 0 5px;
  border-radius: 2px;
  color: rgb(43,43,43);
`;

const CheckboxContainer = styled('div')`
  margin-top: 5px;
`;

export class ApplicationSettings extends Component {
    state = {
        reveal: false,
        data: {
            name: '',
            description: '',
            app_id: this.props.match.params.id,
            allowed_callback_urls: '',
            secret: '',
            application_login_uri: '',
            allowed_origins: '',
            allowed_cors: '',
            terms_of_use: '',
            privacy_policy: '',
            logo: defaultImg,
        }
    };

    setAppData = (app) => {
        this.setState((state) => ({
            data: {
                ...state.data,
                name: app.name,
                description: app.description,
                allowed_callback_urls: app.allowed_callback_urls.join(', '),
                app_id: app.app_id,
                secret: app.secret,
                application_login_uri: app.application_login_uri,
                allowed_origins: app.allowed_origins.join(', '),
                allowed_cors: app.allowed_cors.join(', '),
                logo: app.logo,
                terms_of_use: app.terms_of_use,
                privacy_policy: app.privacy_policy,
            }
        }));
    };

    async componentDidMount() {
        let app = this.props.getAppById(this.props.match.params.id);

        if (app) {
            this.setAppData(app)
        } else {
            await this.props.getApplications();

            app = this.props.getAppById(this.props.match.params.id);

            this.setAppData(app)
        }
    }

    handleSaveGeneral = () => {
        let payload = {};

        this.state.data.name && (payload.name = this.state.data.name);
        this.state.data.description && (payload.description = this.state.data.description);
        this.state.data.allowed_origins && (payload.allowed_origins = this.state.data.allowed_origins.split(',').map(item => item.trim()));
        this.state.data.allowed_cors && (payload.allowed_cors = this.state.data.allowed_cors.split(',').map(item => item.trim()));

        this.props.updateApplication(payload, this.state.data.app_id);
    };

    handleInput = (value, name) => {
        this.setState((state) => ({
            data: {
                ...state.data,
                [name]: value
            }
        }));
    };

    handleReveal = (value) => {
        this.setState({
            reveal: value,
        })
    };

    render() {
        return (
            <Wrapper>
                <Card title="General">
                    <TextInputFiled
                        placeholder="Web application"
                        name="name"
                        defaultValue={this.state.data.name}
                        onChange={this.handleInput}
                    >
                        Name
                    </TextInputFiled>
                    <CopyInputFiled
                        defaultValue={this.state.data.app_id}
                    >
                        Client ID
                    </CopyInputFiled>
                    <CopyInputFiled
                        defaultValue={this.state.data.secret}
                        description="The Client Secret is not base64 encoded"
                        hidden={!this.state.reveal}
                        content={
                            <CheckboxContainer>
                                <CheckboxField
                                    name="reveal_secret"
                                    label="Reveal client secret"
                                    value={this.state.reveal}
                                    onChange={this.handleReveal}
                                />
                            </CheckboxContainer>
                        }
                    >
                        Client Secret
                    </CopyInputFiled>
                    <TextareaInputFiled
                        placeholder="This application used for …"
                        description="A free text description of the application. Max character count is 140."
                        name="description"
                        rows={6}
                        onChange={this.handleInput}
                        value={this.state.data.description}
                    >
                        Description
                    </TextareaInputFiled>
                    <TextareaInputFiled
                        description={
                            <Fragment>
                                After the user authenticates we will only call back to any og these URLs. You can
                                specify multiple valid URLs by comma-separating them (typically to handle different
                                environments like QA or testing). Make sure to specify the protocol
                                , <Code>http://</Code> or <Code>https:</Code>, otherwise the callback may fail in some
                                cases.
                            </Fragment>
                        }
                        name="allowed_callback_urls"
                        rows={6}
                        onChange={this.handleInput}
                        value={this.state.data.allowed_callback_urls}
                    >
                        Allowed Callback URLs
                    </TextareaInputFiled>
                    <TextInputFiled
                        description={
                            <Fragment>
                                In some scenarios, Auth0 will need to redirect to your application's login page.
                                This URI needs to point to a route in your application that should redirect to your
                                tenant's <Code>/authorize</Code> endpoint. <a href="#">Learn more</a>
                            </Fragment>
                        }
                        placeholder="https://myapp.org/login"
                        name="application_login_uri"
                        defaultValue={this.state.data.application_login_uri}
                        onChange={this.handleInput}
                    >
                        Application Login URI
                    </TextInputFiled>
                    <TextareaInputFiled
                        value={this.state.data.allowed_cors}
                        rows={6}
                        name="allowed_cors"
                        onChange={this.handleInput}
                        description={
                            <Fragment>
                                Comma-separated list of allowed origins for use
                                with <a href="#">Cross-Origin Authentication</a> and <a href="#">web message response
                                mode</a>,
                                in the form of <Code>{'<scheme> "://" <host> [ ":" <port> ]'}</Code>, such
                                as <Code>https://login.mydomain.com</Code> or <Code>http://localhost:3000</Code>.
                            </Fragment>
                        }
                    >
                        Allowed Web Origins
                    </TextareaInputFiled>
                    <SaveButton onClick={this.handleSaveGeneral}>Save</SaveButton>
                </Card>
                <Card title="Design & Compliance">
                    <CardContainer>
                        <CardInputs>
                            <FileInputFiled
                                logo={this.state.data.logo}
                                name="app_logo"
                                description={<p>The URL of the logo to display for the application, if none is set the
                                    default badge for this type of application will be shown. <br/> Recommended size is
                                    150x150 pixels.</p>}
                            >
                                Application Logo
                            </FileInputFiled>
                            <TextareaInputFiled
                                description="Your Privacy Policy to be shown on consent screen"
                                name="privacy_policy"
                                rows={6}
                                onChange={this.handleInput}
                                value={this.state.data.privacy_policy}
                            >
                                Privacy policy
                            </TextareaInputFiled>
                            <TextareaInputFiled
                                description="Your Terms of Use to be shown on consent screen"
                                name="terms_of_use"
                                rows={6}
                                onChange={this.handleInput}
                                value={this.state.data.terms_of_use}
                            >
                                Terms of Use
                            </TextareaInputFiled>
                            <SaveButton>Save</SaveButton>
                        </CardInputs>
                        <CardPhone>
                            <Phone name={this.state.data.name} logo={this.state.data.logo}/>
                        </CardPhone>
                    </CardContainer>
                </Card>
            </Wrapper>
        );
    }
}

const mapStateToProps = (state, props) => ({
    getAppById: (app_id) => (console.log(state.apps), state.apps && state.apps.filter(app => app.app_id === app_id)[0]),
    application: state.apps && state.apps.filter(app => app.app_id === props.match.params.id)[0]
});

const mapDispatchToProps = dispatch => ({
    getApplications: () => dispatch(getApplications()),
    updateApplication: (payload, appId) => dispatch(updateApplication(payload, appId))
});

export default connect(mapStateToProps, mapDispatchToProps)(ApplicationSettings);
