import React from 'react';
import styled from 'styled-components';

import phoneImg from '../../assets/phone.png';

const Wrapper = styled('div')`
  max-width: 223px;
  position: relative;
  
  img {
    width: 100%;
  }
`;

const Content = styled('div')`
  position: absolute;
  display: flex;
  flex-direction: column;
  align-items: center;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  padding: 55px 19px 0;
  
  img {
    width: 48px;
  }
`;

const Text = styled('p')`
  fontSize: 9px;
  color: #3E466D;
  text-align: center;
  line-height: 14px;
  
  strong {
    fontWeight: bold;
  }
`;

export const Phone = ({name, logo}) => (
    <Wrapper>
        <PhoneMarkup name={name} logo={logo}/>
    </Wrapper>
);

const PhoneMarkup = ({name, logo}) => (
    <svg width="236" height="476" xmlns="http://www.w3.org/2000/svg">
        <defs>
            <rect id="a" width="13.67" height="13.67" rx="6.83"/>
            <rect id="c" width="41" height="41" rx="8.75"/>
            <rect id="d" width="41" height="41" rx="8.75"/>
        </defs>
        <g fill="none" fillRule="evenodd">
            <g>
                <path
                    d="M36.86 1.96A33.44 33.44 0 0 0 3.42 35.4v404.52a33.44 33.44 0 0 0 33.44 33.44h162.28a33.44 33.44 0 0 0 33.44-33.44V35.4a33.44 33.44 0 0 0-33.44-33.44H36.86z"
                    stroke="#95A0D2" strokeWidth="3.87"/>
                <path
                    d="M38.14 459.92c-16.03 0-23.77-8.81-23.77-23.66V37.41c0-19.25 12.72-24.2 25.42-24.2h17.83a2.76 2.76 0 0 1 2.62 2.6c0 8.54 4.84 13.9 12.72 13.9h90.08c7.88 0 12.72-5.36 12.72-13.9a2.76 2.76 0 0 1 2.62-2.6h17.83c12.7 0 25.42 4.95 25.42 24.2v398.85c0 14.85-7.75 23.66-23.77 23.66H38.14zM19.3 37.67v404.36h197.35V37.67H19.3z"
                    fill="#FFF"/>
                <path
                    d="M1.1 141.26h.39v34.82h-.38c-.62 0-1.11-.5-1.11-1.1v-32.61c0-.61.5-1.1 1.1-1.1zM1.1 97.71h.39v34.82h-.38c-.62 0-1.11-.5-1.11-1.1V98.81c0-.61.5-1.11 1.1-1.11zM1.1 63.64h.39V82h-.38c-.62 0-1.11-.5-1.11-1.1V64.74c0-.61.5-1.1 1.1-1.1zM234.51 109.32h.38c.62 0 1.11.5 1.11 1.1v53.2c0 .6-.5 1.1-1.1 1.1h-.39v-55.4z"
                    fill="#95A0D2"/>
                <rect fill="#95A0D2" x="81.25" y="453.21" width="74.06" height="2.76" rx="1.38"/>
                <path
                    d="M197.86 460.34H38.14c-15.9 0-24.27-8.62-24.27-24.15V37.46c0-17.17 9.85-24.7 25.92-24.7l17.86.01c1.67.08 3 1.41 3.1 3.1 0 8.3 4.64 13.4 12.2 13.4h90.1c7.56 0 12.2-5.1 12.2-13.42.1-1.66 1.43-3 3.13-3.08h17.83c16.07 0 25.92 7.52 25.92 24.7v398.72c0 15.53-8.37 24.15-24.27 24.15z"
                    stroke="#95A0D2" fillOpacity=".25" fill="#FFF"/>
                <g opacity=".8" transform="translate(18.24 19.9)">
                    <path
                        d="M176.03 2.8c1.05 0 2.05.44 2.8 1.2.06.06.15.06.2 0l.55-.59a.17.17 0 0 0 .05-.11.17.17 0 0 0-.05-.11 4.88 4.88 0 0 0-7.1 0 .17.17 0 0 0-.04.11c0 .04.02.08.04.11l.55.6c.05.05.15.05.2 0a3.92 3.92 0 0 1 2.8-1.2zm0 1.92c.58 0 1.13.23 1.55.64.06.06.15.06.2 0l.55-.6a.17.17 0 0 0 .04-.1.17.17 0 0 0-.04-.12 3.2 3.2 0 0 0-4.59 0 .17.17 0 0 0-.05.11c0 .05.02.09.05.12l.54.59c.06.06.15.06.2 0 .43-.41.98-.64 1.55-.64zm1.05 1.4a.16.16 0 0 0 .04-.11.16.16 0 0 0-.05-.12 1.53 1.53 0 0 0-2.08 0 .16.16 0 0 0-.05.12c0 .04.02.08.05.11l.94 1.02c.03.03.06.05.1.05.04 0 .08-.02.1-.05l.95-1.02zM162.43 4.77h.48c.27 0 .5.2.5.46v.94c0 .25-.23.46-.5.46h-.48a.48.48 0 0 1-.5-.46v-.94c0-.25.23-.46.5-.46zm2.27-.94h.49c.27 0 .49.21.49.47v1.87c0 .25-.22.46-.49.46h-.49a.48.48 0 0 1-.49-.46V4.3c0-.26.22-.47.5-.47zm2.28-1.08h.49c.27 0 .48.2.48.46v2.96c0 .25-.21.46-.48.46h-.5a.48.48 0 0 1-.48-.46V3.2c0-.26.22-.46.49-.46zm2.27-1.1h.5c.26 0 .48.22.48.47v4.05c0 .25-.22.46-.49.46h-.49a.48.48 0 0 1-.48-.46V2.12c0-.25.21-.46.48-.46z"
                        fill="#95A0D2" fillRule="nonzero"/>
                    <text fill="#95A0D2" fontFamily="Helvetica" fontSize="7.74" letterSpacing="-.15">
                        <tspan x="15.44" y="8">7:23</tspan>
                    </text>
                    <rect stroke="#FFF" strokeWidth=".55" x="182.11" y="1.93" width="10.5" height="4.97" rx="1.47"/>
                    <path
                        d="M193.86 3.64c.42.18.69.6.69 1.06 0 .46-.27.87-.7 1.05v-2.1zm-10.18-.88h7.88c.4 0 .74.33.74.74v2.4c0 .4-.33.73-.74.73h-7.88a.74.74 0 0 1-.74-.73V3.5c0-.4.33-.74.74-.74z"
                        fill="#95A0D2" fillRule="nonzero"/>
                </g>
            </g>
            <g transform="translate(15 45)">
                <path d="M0 84h205v28.97H0z"/>
                <g transform="translate(173.84 91.65)">
                    <mask id="b" fill="#fff">
                        <use href="#a"/>
                    </mask>
                    <use fill="#414042" href="#a"/>
                    <path
                        d="M3.9 7.81a.98.98 0 1 1 0-1.95.98.98 0 0 1 0 1.95zm2.93 0a.98.98 0 1 1 0-1.95.98.98 0 0 1 0 1.95zm2.93 0a.98.98 0 1 1 0-1.95.98.98 0 0 1 0 1.95z"
                        fill="#FFF" mask="url(#b)"/>
                </g>
                <path d="M149.24 91.65h13.67v13.67h-13.67z"/>
                <path
                    d="M150.76 104.65h.16c.7 0 1.26-.57 1.27-1.28v-6.4c0-.99.79-1.79 1.77-1.79h6.77v-.06c0-.7-.57-1.27-1.27-1.27h-2.86a.63.63 0 0 1-.46-.2l-1.04-1.14a1.27 1.27 0 0 0-.93-.42h-3.41c-.7 0-1.27.57-1.27 1.28v10c0 .7.56 1.28 1.27 1.28z"
                    fill="#414042"/>
                <path
                    d="M152.76 97.2v6.4c0 .48-.2.94-.54 1.28h9.17c.7 0 1.27-.57 1.27-1.28v-6.4c0-.71-.57-1.28-1.27-1.28h-7.36c-.7 0-1.27.57-1.27 1.27z"
                    fill="#414042"/>
                <path fill="#C9CACC" d="M17.49 112.7H187.5v1H17.49zM17.49 84H187.5v1H17.49z"/>
                <text fontFamily="Lato-Medium, Lato" fontSize="9.84" fontWeight="400" letterSpacing=".08"
                      fill="#414042" transform="translate(0 84)">
                    <tspan x="17.49" y="17.65">{name || 'Default'}</tspan>
                </text>
                <g transform="translate(82 35)">
                    <image href={logo} width="40.45" height="40.45"/>
                </g>
                <g>
                    <path d="M0 138h205v33.35H0z"/>
                    <path fill="#C9CACC" d="M17.49 171.07H205v1H17.49z"/>
                    <g fontFamily="Lato-Medium, Lato" fontWeight="400">
                        <text fontSize="9.84" letterSpacing=".08" fill="#414042" transform="translate(17.5 141.83)">
                            <tspan x="0" y="10">Email address</tspan>
                        </text>
                        <text fontSize="8.75" letterSpacing=".07" fill="#6515DD" transform="translate(17.5 141.83)">
                            <tspan x="0" y="22.67">email@example.com</tspan>
                        </text>
                    </g>
                    <path fill="#8D8C8E" d="M183.13 151.87l.76-.75 3.62 3.55-3.62 3.56-.76-.75 2.85-2.8z"/>
                </g>
                <g>
                    <path d="M0 172h205v33.35H0z"/>
                    <path fill="#C9CACC" d="M17.49 205.07H205v1H17.49z"/>
                    <g fontFamily="Lato-Medium, Lato" fontWeight="400">
                        <text fontSize="9.84" letterSpacing=".08" fill="#414042" transform="translate(17.5 175.83)">
                            <tspan x="0" y="10">Phone number</tspan>
                        </text>
                        <text fontSize="8.75" letterSpacing=".07" fill="#6515DD" transform="translate(17.5 175.83)">
                            <tspan x="0" y="22.67">+11 11 111 1111</tspan>
                        </text>
                    </g>
                    <path fill="#8D8C8E" d="M183.13 185.87l.76-.75 3.62 3.55-3.62 3.56-.76-.75 2.85-2.8z"/>
                </g>
                <g>
                    <path d="M0 118h205v20.77H0z"/>
                    <text fontFamily="Lato-Bold, Lato" fontSize="7.11" fontWeight="bold" letterSpacing=".14"
                          fill="#8D8C8E" transform="translate(0 118)">
                        <tspan x="17.49" y="13.01">DETAILS TO BE SHARED</tspan>
                    </text>
                </g>
                <g>
                    <path d="M0 0h205v35H0z"/>
                    <path
                        d="M192.67 18.6l-4.06 4.07-4.08-4.07-.58.56 4.08 4.08-4.05 4.07.54.58 4.08-4.08 4.06 4.06.59-.56-4.08-4.08 4.06-4.07-.55-.57z"
                        fill="#414042"/>
                </g>
                <rect fill="#6515DD" x="17" y="358" width="170" height="27" rx="4.37"/>
                <text fontFamily="Lato-Medium, Lato" fontSize="7.65" fontWeight="400" letterSpacing=".14">
                    <tspan x="25.38" y="329" fill="#8D8C8E">By continuing, you agree to share the above</tspan>
                    <tspan x="17" y="338.29" fill="#8D8C8E">specified details with Instacart. You also agree to</tspan>
                    <tspan x="38.91" y="347.59" fill="#8D8C8E">our</tspan>
                    <tspan x="52.44" y="347.59" fill="#6515DD">Terms of Use</tspan>
                    <tspan x="97.73" y="347.59" fill="#8D8C8E"> and</tspan>
                    <tspan x="114.68" y="347.59" fill="#6515DD">Privacy Policy</tspan>
                    <tspan x="162.75" y="347.59" fill="#8D8C8E">.</tspan>
                </text>
                <text fontFamily="Lato-Medium, Lato" fontSize="9.29" fontWeight="400" letterSpacing=".14"
                      fill="#FFF">
                    <tspan x="82.39" y="375">Authorize</tspan>
                </text>
            </g>
            <path fill="#FFF" fillRule="nonzero" opacity=".75" d="M15 162h206v279H15z"/>
        </g>
    </svg>
);
