import React from 'react';
import styled from 'styled-components';

const Label = styled('label')`
  display: inline-block;
  color: #6515DD;
  border: 1px solid #6515DD;
  padding: 5px 24px;
  border-radius: 20px;
  cursor: pointer;
  
  input {
    display: none;
  }
`;

export const FileInput = ({name}) => (
    <Label htmlFor={name}>
        <input type="file" id={name} name={name}/>
        Upload
    </Label>
);
