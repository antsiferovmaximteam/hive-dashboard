import React, {Component} from 'react';
import styled from 'styled-components';

import {InputField} from "../templates";
import {TextInput} from '../atoms';
import {Button} from 'ui';
import clipboardImage from 'assets/images/clipboard-image.svg';

const InputWrapper = styled('div')`
  position: relative;
  width: 100%;
`;

const CopyInput = styled(TextInput)`
  &[disabled] {
    background-color: #E6E7ED;
  }
`;

const ClipboardButton = styled(Button)`
  position: absolute;
  top: calc(50% - 8px);
  right: 18px;
  width: 13px;
  height: 16px;
  background: url(${clipboardImage});
  background-position: center;
  background-size: cover;
`;

export class CopyInputFiled extends Component {
    componentDidMount() {
        this.input.addEventListener("copy", (event) => {
            event.preventDefault();

            if (event.clipboardData) {
                event.clipboardData.setData("text/plain", this.input.value);
            }
        })
    }

    onClickClipboard = () => {
        const range = document.createRange();
        const selection = window.getSelection();
        range.selectNode(this.input);

        selection.removeAllRanges();
        selection.addRange(range);

        document.execCommand('copy')
    };

    getInputType() {
        const {hidden = false} = this.props;

        return hidden ? 'password' : 'text';
    }

    render() {
        const {disabled = true} = this.props;

        return (
            <InputField
                label={this.props.children}
                description={this.props.description}
            >
                <InputWrapper>
                    <CopyInput
                        disabled={disabled}
                        type={this.getInputType()}
                        innerRef={el => this.input = el}
                        defaultValue={this.props.defaultValue}
                    />
                    <ClipboardButton onClick={this.onClickClipboard}/>
                </InputWrapper>
                {this.props.content}
            </InputField>
        );
    }
}
