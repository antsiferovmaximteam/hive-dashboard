import React from 'react';
import styled from 'styled-components';

export const TextInputWrapper = styled('div')`
  display: flex;
  margin-top: 40px;
  width: 100%;
`;

export const InputLabel = styled('label')`
  font-weight: 600;
  font-size: 14px;
  color: #3E466D;
`;

export const InputLabelContainer = styled('div')`
  padding-top: 5px;
  padding-right: 24px;
  max-width: 210px;
  width: 100%;
  text-align: right;
`;

export const InputContainer = styled('div')`
  max-width: 548px;
  width: 100%;
`;

export const InputDescription = styled('div')`
  margin: 7px 0 0;
  font-weight: 400;
  font-size: 12px;
  color: #3E466D;
  letter-spacing: 0;
  text-align: left;
  line-height: 18px;
  
  p {
    margin: 0;
    padding: 0;
  }
  
  a {
    color: #6515DD;
  }
`;

export const InputField = ({label, children, description, id}) => (
    <TextInputWrapper>
        <InputLabelContainer>
            <InputLabel htmlFor={id}>{label}</InputLabel>
        </InputLabelContainer>
        <InputContainer>
            {children}
            {description && <InputDescription>{description}</InputDescription>}
        </InputContainer>
    </TextInputWrapper>
);
