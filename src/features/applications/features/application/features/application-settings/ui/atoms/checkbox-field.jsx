import React from 'react';
import styled from 'styled-components';

import {Checkbox} from '../../../../atoms';

const Container = styled('div')`
  display: flex;
  align-items: center;
`;

const Label = styled('label')`
  margin: 0 0 0 5px;
  font-weight: 400;
  font-size: 12px;
  color: #3E466D;
  letter-spacing: 0;
  text-align: left;
  cursor: pointer;
  user-select: none;
`;

const CheckboxContainer = styled('div')`
  label {
    display: flex;
    align-items: center;
    justify-content: center;
    width: 14px;
    height: 14px;
    border: 1px solid rgb(190,190,190);
    border-radius: 3px;
    box-shadow: 0 0 3px -1px rgb(190,190,190) inset;
    transition: all .2s ease-in-out;
    cursor: pointer;
    
    svg {
      margin-top: 1px;
      width: 70%;
      height: 70%;
    }
  }
  
  input {
    display: none;
    
    &:checked ~ label {
      background-color: #6515DD;
      border-color: #6515DD;
      box-shadow: none;
    }
  }
`;

export const CheckboxField = ({name, id = name, label, onChange, value}) => (
    <Container>
        <Checkbox name={name} id={id} onChange={onChange} value={value}/>
        <Label htmlFor={id}>{label}</Label>
    </Container>
);
