import React, {Component} from 'react';
import styled from 'styled-components';
import defaultLogo from 'assets/images/default-logo.svg';

import {FileInput} from "./file-input";
import {InputField} from "../templates";

const SvgContainer = styled('div')`
  display: flex;
  align-items: center;

  &:after {
    content: '';
    display: block;
    width: 1px;
    height: 20px;
    margin: 0 16px;
    background-color: #DEE0E7;
  }
`;

const Row = styled('div')`
  display: flex;
  align-items: center;
`;

export class FileInputFiled extends Component {
    render() {
        return (
            <InputField
                label={this.props.children}
                description={this.props.description}
                id={this.props.name}
            >
                <Row>
                    {this.props.logo !== defaultLogo && <CheckIcon/>}
                    <FileInput
                        name={this.props.name}
                    />
                </Row>
            </InputField>
        );
    }
}

const CheckIcon = () => (
    <SvgContainer>
        <svg width="30" height="30" xmlns="http://www.w3.org/2000/svg">
            <g fill="none" fillRule="evenodd">
                <circle fill="#22CC4E" cx="15" cy="15" r="15"/>
                <g stroke="#FFF" strokeLinecap="round" strokeWidth="3">
                    <path d="M10.31 15.63l3.44 3.43M13.75 19.06l6.88-6.87"/>
                </g>
            </g>
        </svg>
    </SvgContainer>
);
