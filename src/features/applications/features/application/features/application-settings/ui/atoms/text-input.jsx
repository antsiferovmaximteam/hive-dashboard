import styled from 'styled-components';

export const TextInput = styled('input')`
  width: 100%;
  padding: 7px 16px;
  border: 1px solid #DEE0E7;
  border-radius: 4px;
  outline: none;
  font-weight: 400;
  font-size: 14px;
  color: #504A8E;
  letter-spacing: 0;
  text-align: left;
  
  &::placeholder {
    color: #767FAB;
  }
`;

export const TextareaInput = styled('textarea')`
  width: 100%;
  padding: 7px 16px;
  border: 1px solid #DEE0E7;
  border-radius: 4px;
  outline: none;
  font-weight: 400;
  font-size: 14px;
  line-height: 19px;
  color: #504A8E;
  letter-spacing: 0;
  text-align: left;
  max-width: 100%;
  min-width: 100%;
  
  &::placeholder {
    color: #767FAB;
  }
`;
