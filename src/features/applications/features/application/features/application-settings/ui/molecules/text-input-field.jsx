import React from 'react';
import {TextInput, TextareaInput} from "../atoms";
import {InputField} from "../templates";

export const TextInputFiled = ({children, name, description, defaultValue, placeholder, onChange}) => (
    <InputField
        label={children}
        description={description}
        id={name}
    >
        <TextInput
            defaultValue={defaultValue}
            onChange={e => onChange(e.target.value, name)}
            id={name}
            name={name}
            placeholder={placeholder}
        />
    </InputField>
);

export const TextareaInputFiled = ({children, name, disabled, description, value, placeholder, onChange, rows = 1}) => (
    <InputField
        label={children}
        description={description}
        id={name}
    >
        <TextareaInput
            rows={rows}
            onChange={e => onChange(e.target.value, name)}
            id={name}
            name={name}
            placeholder={placeholder}
            value={value}
        />
    </InputField>
);
