import styled from "styled-components";
import {YellowButton} from "ui/atoms";

export const SaveButton = styled(YellowButton)`
  text-transform: uppercase;
  align-self: flex-start;
  padding: 9px 40px;
  margin-top: 40px;
  margin-left: 210px;
  margin-bottom: 40px;
`;