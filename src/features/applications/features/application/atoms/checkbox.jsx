import React from 'react';
import styled from 'styled-components';

const CheckboxContainer = styled('div')`
  label {
    display: flex;
    align-items: center;
    justify-content: center;
    width: 14px;
    height: 14px;
    border: 1px solid rgb(190,190,190);
    border-radius: 3px;
    box-shadow: 0 0 3px -1px rgb(190,190,190) inset;
    transition: all .2s ease-in-out;
    cursor: pointer;
    
    svg {
      margin-top: 1px;
      width: 70%;
      height: 70%;
    }
  }
  
  input {
    display: none;
    
    &:checked ~ label {
      background-color: #6515DD;
      border-color: #6515DD;
      box-shadow: none;
    }
  }
`;

export const Checkbox = ({name, id = name, onChange, value}) => (
    <CheckboxContainer>
        <input type="checkbox" id={id} checked={value} onChange={event => onChange(event.target.checked, name)}/>
        <label htmlFor={id}>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 26">
                <path fill="white" d="M.3 14c-.2-.2-.3-.5-.3-.7s.1-.5.3-.7l1.4-1.4a1 1 0 0 1 1.4 0l.1.1 5.5 5.9c.2.2.5.2.7 0L22.8 3.3h.1a1 1 0 0 1 1.4 0l1.4 1.4c.4.4.4 1 0 1.4l-16 16.6a1 1 0 0 1-.7.3 1 1 0 0 1-.7-.3L.5 14.3.3 14z"/>
            </svg>
        </label>
    </CheckboxContainer>
);
