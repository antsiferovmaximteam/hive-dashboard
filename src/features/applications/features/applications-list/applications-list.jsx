import React, {Component} from 'react';
import { connect } from 'react-redux';

import {ScreenWithHeader} from 'ui';
import {List} from './ui';
import {getApplications} from '../../store';

export class ApplicationsList extends Component {
    componentDidMount() {
        this.props.apps.length === 0 && this.props.getApplications();
    }

    render = () => (
        <ScreenWithHeader
            title="Applications"
            description="Setup a mobile, web or IoT application to use Hive for Authentication"
            modal={true}
        >
            <List applications={this.props.apps}/>
        </ScreenWithHeader>
    )
}

const mapStateToProps = state => ({
    apps: state.apps
});

const mapDispatchToProps = dispatch => ({
    getApplications: () => dispatch(getApplications())
});

export default connect(mapStateToProps, mapDispatchToProps)(ApplicationsList);