import React from 'react';
import styled from 'styled-components';

const Svg = styled('svg')`
  padding: 0 7px;
  box-sizing: content-box;
  color: #95A0D2;
  transition: all .2s;
  cursor: pointer;
  
  &:last-child {
    margin-right: 0;
  }
  
  &:hover {
    color: #7956EF;
  }
`;

export const AppIcon = ({children}) => (
    <Svg width="16px" height="16px" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg">
        <g stroke="none" strokeWidth="1" fillRule="evenodd" fill="transparent">
            {children}
        </g>
    </Svg>
);