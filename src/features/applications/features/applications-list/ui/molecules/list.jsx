import React from 'react';
import styled from 'styled-components';

import {WhiteCard} from 'ui/index';
import {ApplicationItem, ApplicationItemEmpty} from '../atoms';

const ApplicationsWrapper = styled(WhiteCard)`
  padding: 0;
`;

const AppList = styled('ul')`
  list-style: none;
  margin: 0;
  padding: 0;
`;

export const List = ({applications}) => (
    <ApplicationsWrapper>
        <AppList>
            {(!applications || applications.length === 0) && <ApplicationItemEmpty/>}
            {applications.map && applications.map(app => (
                <ApplicationItem key={app.app_id} {...app}/>
            ))}
        </AppList>
    </ApplicationsWrapper>
);

List.defaultProps = {
    applications: []
};