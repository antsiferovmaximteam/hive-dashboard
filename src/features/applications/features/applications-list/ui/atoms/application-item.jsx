import React from 'react';
import styled from 'styled-components';
import {Link} from 'react-router-dom';

import {IconSettings, IconScopes, IconPermissions, IconRemove} from '../../assets/app-control-icons';
import tempIcon from '../../assets/temp-default-icon.svg';

const ListItem = styled('li')`
  display: flex;
  align-items: center;
  padding: 11px 24px;
  border-bottom: 1px solid #F4F6FF;
  transition: all .1s;
  
  &:last-child {
    border-bottom: none;
  }
  
  &:hover {
    background-color: #fbfbfb;
  }
`;

const AppIcon = styled('div')`
  width: 27px;
  height: 27px;
  //border: .5px solid rgba(149, 160, 210, .5);
  //padding: 4px;
  //border-radius: 5px;
  //
  img {
    width: 100%;
  }
`;

const AppName = styled('div')`
  margin-left: 14px;
  font-weight: 600;
  font-size: 14px;
  color: #3E466D;
  line-height: 22px;
  width: 40%;
`;

const AppID = styled('div')`
  font-weight: 400;
  font-size: 12px;
  color: #504A8E;
  line-height: 22px;
  
  strong {
    font-weight: 600;
    font-size: 14px;
    color: #3E466D;
    line-height: 22px;
  }
`;

const Icons = styled('div')`
  margin-left: auto;
  min-width: 136px;
  padding-left: 20px;
`;

export const ApplicationItem = ({logo, name, app_id}) => (
    <ListItem>
        <AppIcon>
            <Link to={`/applications/${app_id}/settings`}>
                <img src={logo} alt={name}/>
            </Link>
        </AppIcon>
        <AppName>
            <Link to={`/applications/${app_id}/settings`}>{name}</Link>
        </AppName>
        <AppID>AppId: <strong>{app_id}</strong></AppID>
        <Icons>
            <Link to={`/applications/${app_id}/settings`}><IconSettings/></Link>
            <IconScopes/>
            {/*<IconPermissions/>*/}
            <IconRemove/>
        </Icons>
    </ListItem>
);

ApplicationItem.defaultProps = {
  icon: tempIcon
};
