import React from 'react';
import styled from 'styled-components';
import emptyIcon from '../../assets/applications-empty-icon.svg';

const EmptyItem = styled('li')`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 40px;
  
  img {
    width: 56px;
  }
  
  p {
    margin-top: 16px;
    font-weight: 600;
    font-size: 14px;
    color: #3E466D;
    text-align: center;
    line-height: 22px;
    max-width: 337px;
  }
`;

export const ApplicationItemEmpty = () => (
    <EmptyItem>
        <img src={emptyIcon} alt="Not applications"/>
        <p>When you find a store you love, tap the heart and we’ll list it for you here.</p>
    </EmptyItem>
);