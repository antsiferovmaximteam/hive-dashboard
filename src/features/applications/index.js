import React from 'react';
import {Route, Switch} from 'react-router-dom';
import ApplicationsList from './features/applications-list/applications-list';
import {Application} from "./features/application";
import {routes} from "routes";

export const Applications = () => (
    <Switch>
        <Route exact path={routes.root.applications.path} component={ApplicationsList}/>
        <Route path={routes.root.applications.application.path} component={Application}/>
    </Switch>
);