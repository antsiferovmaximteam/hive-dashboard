import {createActions, handleActions} from'redux-actions';
import {Api} from "api";

export const {getApplications, updateApplication, createApplication} = createActions({
    GET_APPLICATIONS: async () => {
        const {data} = await Api.getApplications();

        return data;
    },
    UPDATE_APPLICATION: async (payload, appId) => {
        const {data} = await Api.updateApplication(payload, appId);

        return data;
    },
    CREATE_APPLICATION: async (payload) => {
        const {data} = await Api.createApplication(payload);

        return data;
    }
});

export const reducer = handleActions({
    [getApplications]: (state, {payload}) => payload,
    [updateApplication]: (state, {payload}) => state.map(app => app.app_id === payload.app_id ? payload : app),
    [createApplication]: (state, {payload}) => [...state, payload]
}, []);
