import React from 'react';
import styled from 'styled-components';
import {Link} from 'react-router-dom';

import rightArrow from 'assets/images/right-arrow.svg';

const css = String.raw;

const Breadcrumbs = styled('ul')`
  display: flex;
  align-items: baseline;
  list-style: none;
  padding: 0;
  margin: 0;
`;

const BreadcrumbsItem = styled('li')`
  font-weight: 600;

  ${p => !p.last && css`
      display: flex;
      align-items: center;
      font-size: 22px;
      color: #6515DD;
      letter-spacing: 0;
      line-height: 32px;
      
      &:after {
        content: '';
        display: block;
        height: 8px;
        width: 8px;
        margin: 0 13px;
        background: url(${rightArrow});
        background-size: cover;
        background-position: center;
      }
  `};
  
  ${p => p.last && css`
      font-size: 32px;
      color: #3E466D;
      letter-spacing: 0;
      line-height: 48px;
  `}
`;

const Item = ({children, path}) => (
    path ?
        <BreadcrumbsItem><Link to={path}>{children}</Link></BreadcrumbsItem> :
        <BreadcrumbsItem last={true}>{children}</BreadcrumbsItem>
);

Breadcrumbs.Item = Item;

export {Breadcrumbs};
