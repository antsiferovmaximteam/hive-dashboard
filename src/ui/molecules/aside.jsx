import React from 'react';
import styled from 'styled-components';

import {HiveLogo, Navigation} from 'ui';

const AsideWrapper = styled('aside')`
  padding-top: 68px;
  padding-left: 28.8%;
  padding-right: 24px;
`;

const NavContainer = styled('nav')`
  margin-top: 34px;
`;

const LogoContainer = styled('div')`
  margin-left: 8px;
`;

export const Aside = () => (
 <AsideWrapper>
   <LogoContainer>
     <HiveLogo/>
   </LogoContainer>
   <NavContainer>
     <Navigation/>
   </NavContainer>
 </AsideWrapper>
);