import React from 'react';
import {Route, Redirect} from 'react-router-dom';

import {routes} from "routes";

export const PrivateRoute = ({component: Component, ...props}) => {
    const isAuth = !!JSON.parse(localStorage.getItem('auth'));

    return (
        <Route
            {...props}
            render={props =>
                isAuth ?
                    <Component {...props}/> :
                    <Redirect to={{
                        pathname: routes.auth.login.path,
                        state: { from: props.location }
                    }}/>
            }
        />
    )
};
