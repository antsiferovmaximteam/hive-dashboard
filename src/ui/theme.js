import {generateMedia} from "styled-media-query";
import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';

import {Fonts} from 'assets/fonts';
import {injectGlobal} from "styled-components";

const css = String.raw;
export const media = generateMedia({
    huge: "1600px",
});

export const font = {
    main: 'Lato, sans-serif'
};

const globalStyles = css`
  ${generationFontFace(Fonts)}
  
  html {
    font-size: 100%;
    font-family: ${font.main};
    -webkit-font-smoothing: antialiased;
    margin: 0;
    padding: 0;
    background-color: #F8F9FD;
  }
  
  a {
    color: inherit;
    text-decoration: none;
    
    &:hover {
      color: inherit;
      text-decoration: none;
    }
  }
  
  * {
    box-sizing: border-box;
  }
  
  h1, h2, h3, h4 {
    margin: 0;
  }
  
  ul, li {
    margin: 0;
    padding: 0;
  }
`;

const normalize = css`
html{line-height:1.15;-webkit-text-size-adjust:100%;}body{margin:0;}main{display:block;}h1{font-size:2em;margin:0.67em 0;}hr{box-sizing:content-box;height:0;overflow:visible;}pre{font-family:monospace,monospace;font-size:1em;}a{background-color:transparent;}abbr[title]{border-bottom:none;text-decoration:underline;text-decoration:underline dotted;}b,strong{font-weight:bolder;}code,kbd,samp{font-family:monospace,monospace;font-size:1em;}small{font-size:80%;}sub,sup{font-size:75%;line-height:0;position:relative;vertical-align:baseline;}sub{bottom:-0.25em;}sup{top:-0.5em;}img{border-style:none;}button,input,optgroup,select,textarea{font-family:inherit;font-size:100%;line-height:1.15;margin:0;}button,input{overflow:visible;}button,select{text-transform:none;}button,[type="button"],[type="reset"],[type="submit"]{-webkit-appearance:button;}button::-moz-focus-inner,[type="button"]::-moz-focus-inner,[type="reset"]::-moz-focus-inner,[type="submit"]::-moz-focus-inner{border-style:none;padding:0;}button:-moz-focusring,[type="button"]:-moz-focusring,[type="reset"]:-moz-focusring,[type="submit"]:-moz-focusring{outline:1px dotted ButtonText;}fieldset{padding:0.35em 0.75em 0.625em;}legend{box-sizing:border-box;color:inherit;display:table;max-width:100%;padding:0;white-space:normal;}progress{vertical-align:baseline;}textarea{overflow:auto;}[type="checkbox"],[type="radio"]{box-sizing:border-box;padding:0;}[type="number"]::-webkit-inner-spin-button,[type="number"]::-webkit-outer-spin-button{height:auto;}[type="search"]{-webkit-appearance:textfield;outline-offset:-2px;}[type="search"]::-webkit-search-decoration{-webkit-appearance:none;}::-webkit-file-upload-button{-webkit-appearance:button;font:inherit;}details{display:block;}summary{display:list-item;}template{display:none;}[hidden]{display:none;}
`;

export const globalStyled = `${normalize} ${globalStyles}`;

injectGlobal([globalStyled]);

function generationFontFace(fonts) {
    return fonts.reduce((acc, item) => {
        return acc + css`
          @font-face {
            font-family: 'Lato';
            src: url(${item.eot});
            src: url(${item.woff2}) format('woff2'),
            url(${item.woff}) format('woff'),
            url(${item.svg}) format('svg');
            font-weight: ${item.weight};
            font-style: normal;
          }
        `;
    }, '');
}
