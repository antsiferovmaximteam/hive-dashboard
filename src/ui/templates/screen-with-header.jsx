import React, {Component, Fragment} from 'react';
import styled from 'styled-components';

import {MainScreen} from './main-screen';
import {Modal, ApplicationModalContent, YellowButton, ScreenHeader} from 'ui';

const LeftContent = styled('div')``;

const Description = styled('p')`
  margin: 1px 0 8px;
  min-height: 22px;
  font-weight: 600;
  font-size: 14px;
  color: #504A8E;
  letter-spacing: 0;
  line-height: 22px;
`;

const NewAppButton = styled(YellowButton)`
  align-self: flex-end;
`;

export class ScreenWithHeader extends Component {
    state = {
        show: false
    };

    onClose = () => this.setState(() => ({show: false}));
    onOpen = () => this.setState(() => ({show: true}));

    render() {
        return (
            <MainScreen
                header={<Header {...this.props} show={this.state.show} onClose={this.onClose} onOpen={this.onOpen}/>}
            >
                {this.props.children}
            </MainScreen>
        );
    }
}

const Header = ({title, description, modal, show, onClose, onOpen}) => (
    <Fragment>
        <LeftContent>
            <ScreenHeader>{title}</ScreenHeader>
            <Description>{description}</Description>
        </LeftContent>
        {modal && (
            <Fragment>
                <NewAppButton onClick={onOpen}>New Application</NewAppButton>
                <Modal
                    isOpen={show}
                    onClose={onClose}
                >
                    <ApplicationModalContent onClose={onClose}/>
                </Modal>
            </Fragment>
        )}
    </Fragment>
);