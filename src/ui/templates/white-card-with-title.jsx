import React from 'react';
import styled from 'styled-components';

import {WhiteCard} from 'ui';

const Card = styled(WhiteCard)`
  display: flex;
  flex-direction: column;
  
  ${p => p.width && `width: ${p.width}`}
`;

const Title = styled('h3')`
  width: 100%;
  padding: 24px;
  text-align: left;
  font-weight: bold;
  font-size: 16px;
  color: #3E466D;
  letter-spacing: 0;
  line-height: 12px;
  ${p => p.border && 'border-bottom: 1px solid #F4F6FF;'}
`;

const Content = styled('div')`
  padding: 0 24px;
`;

export const WhiteCardWithTitle = ({title, children, borderedTitle = false, ...props}) => (
    <Card {...props}>
        <Title border={borderedTitle}>{title}</Title>
        <Content>
            {children}
        </Content>
    </Card>
);
