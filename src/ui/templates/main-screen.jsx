import React from 'react';
import styled from 'styled-components';

const Wrapper = styled('div')`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  width: 100%;
  height: 100%;
`;

const Header = styled('header')`
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  width: 100%;
  min-height: 99px;
  padding-bottom: 20px;
`;

const Content = styled('div')`
  width: 100%;
  height: 100%;
  padding-top: 19px;
  padding-bottom: 20px;
  overflow-y: scroll;
`;

export const MainScreen = ({header, children}) => (
    <Wrapper>
        <Header>
            {header}
        </Header>
        <Content>
            {children}
        </Content>
    </Wrapper>
);