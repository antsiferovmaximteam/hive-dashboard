import styled from "styled-components";

export const ScreenHeader = styled('h3')`
  font-weight: bold;
  font-size: 32px;
  color: #3E466D;
  letter-spacing: 0;
  line-height: 48px;
`;