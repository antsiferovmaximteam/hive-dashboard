import styled from 'styled-components';

export const WhiteCard = styled('div')`
  width: 100%;
  box-shadow: 0 2px 4px 0 rgba(0,0,0,0.06);
  background-color: #fff;
`;