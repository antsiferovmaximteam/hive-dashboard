import React from 'react';
import styled from 'styled-components';
import logo from 'assets/images/logotype.svg';

const LogoWrapper = styled('div')`
  width: 79px;
  
  img {
    width: 100%;
  }
`;

export const HiveLogo = () => (
    <LogoWrapper>
      <img src={logo} alt="Hive"/>
    </LogoWrapper>
);