import styled from 'styled-components';

export const Button = styled('button')`
  background: none;
  border: none;
  outline: none;
  cursor: pointer;
`;

export const YellowButton = styled(Button)`
  height: 40px;
  padding: 0 24px;
  background-color: #FFB300;
  border-radius: 20px;
  font-weight: 800;
  font-size: 14px;
  color: #FFFFFF;
  letter-spacing: 0;
  text-align: left;
  line-height: 22px;
`;