import styled from 'styled-components';

export const SelectContainer = styled('div')`
  display: inline;
  position: relative;
  width: 54px;
  height: 27px;
  
  @-moz-document url-prefix() {
    &:after {
      right: 15%;
      top: 63%;
    }
  }
  
  select {
    padding: 6px 22px 6px 15px;
    background: #fff;
    border: 1px solid #EDF0FE;
    outline: none;
    border-radius: 18px;
    appearance: none;
    font-weight: 600;
    font-size: 12px;
    color: #7956EF;
    letter-spacing: 0;
    text-align: left;
    line-height: 18px;
  }
`;