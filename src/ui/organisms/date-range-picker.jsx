import React, {Component} from 'react';
import styled from 'styled-components';
// import { DateRangePicker as Picker } from 'react-dates';
import Picker from 'react-dates/lib/components/DateRangePicker';
import moment from 'moment';

import triangle from 'assets/images/triangle.svg';

const DateRangeWrapper = styled('div')`
  display: inline-block;

  .DayPicker {
    border-radius: 8px;
    border: 1px solid #EDF0FE;
    background: #FFFFFF;
    box-shadow: 0 2px 15px 0 rgba(87,43,233,0.19);
  }

  .DateRangePickerInput {
     background: transparent;
     
     * {
      color: inherit !important;
     }
  } 

  .DateRangePickerInput__withBorder {
    border-color: transparent;
  } 

  .DateInput_fang {
    display: none;
  }
  
  .CalendarMonth_table {
    border-collapse: separate;
    border-spacing: 0 2px;
  }

  .CalendarMonth_caption {
    color: #504A8E;
    font-weight: 600;
    font-size: 16px;
    text-align: center;
    line-height: 24px;
  }

  .DayPicker_weekHeader_li {
    font-weight: 600;
    font-size: 14px;
    color: #504A8E;
    border-bottom: 1px solid #ECF0FF;
  }

  .CalendarDay {
    border-color: transparent;
    font-weight: 600;
    font-size: 14px;
    color: #767FAB;
    margin: 3px 0;
  }
  
  .CalendarDay__selected, .CalendarDay__selected:active, .CalendarDay__selected:hover {
    font-size: 14px;
    color: #FFFFFF;
    background: #7956EF;
    border-radius: 50%;
  }
  
  .CalendarDay__blocked_out_of_range, .CalendarDay__blocked_out_of_range:active, .CalendarDay__blocked_out_of_range:hover {
    color: #bcc8ff;
  }
  
  .CalendarDay__default:hover {
    border-color: transparent;
    background: #BCAAF7;
    color: white;
    border-radius: 50%;
  }

  .CalendarDay__selected_span, .CalendarDay__hovered_span {
    background: #BCAAF7;
    color: white;
    border-radius: 0;
    
    &:first-child {
      border-top-left-radius: 50%;
      border-bottom-left-radius: 50%;
    }
    
    &:last-child {
      border-top-right-radius: 50%;
      border-bottom-right-radius: 50%;
    }
  }

  .CalendarDay__selected_start {
    border-bottom-right-radius: 0;
    border-top-right-radius: 0;
  }

  .CalendarDay__selected_end {
    border-bottom-left-radius: 0;
    border-top-left-radius: 0;
  }

  .DateRangePickerInput_arrow {
    padding-bottom: 3px;
  }

  .DateRangePickerInput {
    border-radius: 20px;
  }

  .DateInput {
    width: 97px;
    background: transparent;
  }
  
  .DateInput_input {
    padding: 3px 11px;
    border: none;
    width: 97px;
    background: transparent;
    font-weight: 600;
    font-size: 12px;
    color: #95A0D2;
    letter-spacing: 0;
    text-align: left;
    line-height: 18px;
  }
`;

const Arrow = styled('span')`
  font-weight: 600;
  font-size: 12px;
  color: #95A0D2;
`;

const NavNext = styled('div')`
  position: absolute;
  right: 22px;
  top: 18px;
  height: 30px;
  width: 30px;
  border: 1px solid #ECF0FF;
  border-radius: 50%;
  outline: none;
  background: white;
  
  &:after {
    content: '';
    display: block;
    position: absolute;
    top: calc(50% - 4.5px);
    left: calc(50% - 2.5px);
    width: 6px;
    height: 9px;
    background: url(${triangle}) center;
    background-size: cover;
  }
`;

const NavPrev = styled(NavNext)`
  right: auto;
  left: 22px;

  &:after {
    transform: rotate(180deg);
    top: calc(50% - 4.5px);
    left: calc(50% - 4px);
  }
`;

export class DateRangePicker extends Component {
    state = {
        startDate: moment().subtract(10, 'days'),
        endDate: moment(),
        focusedInput: null
    };


    render() {
        return (
            <DateRangeWrapper>
                <Picker
                    startDate={this.state.startDate} // momentPropTypes.momentObj or null,
                    startDateId="start-day" // PropTypes.string.isRequired,
                    endDate={this.state.endDate} // momentPropTypes.momentObj or null,
                    endDateId="end-day" // PropTypes.string.isRequired,
                    onDatesChange={({ startDate, endDate }) => this.setState({ startDate, endDate })} // PropTypes.func.isRequired,
                    focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
                    onFocusChange={focusedInput => this.setState({ focusedInput })} // PropTypes.func.isRequired,
                    displayFormat="ll"
                    customArrowIcon={<Arrow>-</Arrow>}
                    navNext={<NavNext role="button" tabIndex="0"/>}
                    navPrev={<NavPrev role="button" tabIndex="0"/>}
                    hideKeyboardShortcutsPanel={true}
                    isOutsideRange={() => false}
                    onClose={this.props.onUpdate}
                    transitionDuration={0}
                />
            </DateRangeWrapper>
        );
    }
}