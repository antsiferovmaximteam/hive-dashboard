import React, {Component} from 'react';
import styled from 'styled-components';
import {connect} from 'react-redux';

import {Button} from 'ui';
import closeIcon from 'assets/images/close-icon.svg';
import {createApplication} from 'features/applications/store';

const css = String.raw;

const Wrapper = styled('form')`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  padding: 56px 72px;
`;

const Header = styled('h2')`
  font-weight: 900;
  font-size: 20px;
  color: #3E466D;
  line-height: 30px;
`;

const Label = styled('label')`
  font-weight: bold;
  font-size: 14px;
  color: #3E466D;
  letter-spacing: 0;
  text-align: left;
  line-height: 18px;
`;

const NameContainer = styled('div')`
  display: flex;
  flex-direction: column;
  width: 100%;
  margin-top: 48px;
`;

const Input = styled('input')`
  margin-top: 20px;
  padding: 8px 16px;
  border-radius: 4px;
  border: 1px solid #DEE0E7;
  font-weight: 400;
  font-size: 14px;
  color: #504A8E;
  letter-spacing: 0;
  text-align: left;
  line-height: 22px;
  outline: none;
  transition: all .2s;
  
  &:focus {
    border-color: rgba(121, 86, 239, .5);
  }
`;

const Description = styled('p')`
  margin-top: 8px;
  font-weight: 400;
  font-size: 12px;
  color: #3E466D;
  line-height: 18px;
`;

const TypesContainer = styled('div')`
  margin-top: 48px;
  width: 100%;
`;

const TypesList = styled('ul')`
  display: flex;
  justify-content: space-between;
  list-style: none;
  padding: 0;
  margin: 20px 0 0;
`;

const TypeTitle = styled('h3')`
  font-weight: bold;
  color: #3E466D;
`;

const TypeDescription = styled('p')`
  margin: 16px 0 0;
  color: #504A8E;
`;

const TypeTechnology = styled('p')`
  margin-top: 38px;
  opacity: 0.7;
  color: #504A8E;
`;

const TypeItem = styled('li')`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 25px;
  width: 30%;
  border-radius: 4px;
  border: 1px solid #DEE0E7;
  cursor: pointer;
  line-height: 22px;
  text-align: center;
  letter-spacing: 0;
  font-weight: 400;
  font-size: 14px;
  transition: all .3s;
  overflow: hidden;
  
  background-color: ${p => p.active && '#7956EF'};
  
  // Active  
  ${TypeTitle} {
    color: ${p => p.active && '#FFFFFF'};
  }
  
  ${TypeDescription} {
    color: ${p => p.active && 'rgba(255, 255, 255, .9)'};
  }
  
  ${TypeTechnology} {
    color: ${p => p.active && 'rgba(255, 255, 255, .7)'};
  }
  
  // Disable
  ${p => p.disable && 'cursor: default'};
  ${TypeTitle} {
    color: ${p => p.disable && 'rgba(62, 70, 109, .5)'};
  }
  
  ${TypeDescription} {
    color: ${p => p.disable && 'rgba(80, 74, 142, .5)'};
  }
  
  ${TypeTechnology} {
    color: ${p => p.disable && 'rgba(80, 74, 142, .3)'};
  }
  
  ${p => p.disable && css`
    &:before {
      content: 'Soon';
      position: absolute;
      display: flex;
      justify-content: center;
      padding: 3px;
      width: 100%;
      top: 12px;
      left: -70px;
      transform: rotate(-45deg);
      color: white;
      background-color: #FFB300;
    }
  `}
`;

const CreateButton = styled(Button)`
  margin-top: 48px;
  padding: 10px 33px;
  background-color: #FFB300;
  border-radius: 50px;
  font-weight: 900;
  font-size: 14px;
  color: #FFFFFF;
  text-transform: uppercase;
`;

const CloseButton = styled(Button)`
  position: absolute;
  top: 26px;
  right: 32px;
  width: 16px;
  height: 16px;
  background: url(${closeIcon}) center;
  background-size: cover;
`;

export class ApplicationModalContent extends Component {
    state = {
        type: 'FOR_CONSUMERS',
        name: ''
    };


    handleInput = (value, name) => {
        this.setState(() => ({
            [name]: value
        }));
    };

    handleCreate = async (e) => {
        e.preventDefault();

        await this.props.createApplication({
            name: this.state.name,
            type: this.state.type
        });

        this.props.onClose();
    };

    render() {
        return (
            <Wrapper>
                <CloseButton onClick={this.props.onClose}/>
                <Header>Create New Application</Header>
                <NameContainer>
                    <Label htmlFor='app_name'>Name</Label>
                    <Input type="text" id='app_name' placeholder="Enter your app name"
                           onChange={e => this.handleInput(e.target.value, 'name')}/>
                    <Description>You can change the application name later in the application settings.</Description>
                </NameContainer>
                <TypesContainer>
                    <Label>Choose an application type</Label>
                    <TypesList>
                        {types.map(item => (
                            <TypeItem key={item.id} active={this.state.type === item.value} disable={item.disable}
                                      onClick={() => !item.disable && this.handleInput(item.value, 'type')}>
                                <TypeTitle>{item.title}</TypeTitle>
                                <TypeDescription>{item.description}</TypeDescription>
                                <TypeTechnology>{item.technology}</TypeTechnology>
                            </TypeItem>
                        ))}
                    </TypesList>
                </TypesContainer>
                <CreateButton onClick={this.handleCreate}>Create</CreateButton>
            </Wrapper>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    createApplication: (payload) => dispatch(createApplication(payload))
});

export default connect(null, mapDispatchToProps)(ApplicationModalContent);

const types = [
    {
        title: 'Single Page Web Applications',
        description: 'A JavaScript front-end app that uses an API.',
        technology: 'e.g.: React, Vue.js',
        id: 1,
        disable: false,
        value: 'FOR_CONSUMERS'
    },
    {
        title: 'Regular Web Applications',
        description: 'Traditional web app (with refresh).',
        technology: 'e.g.: Java ASP.NET',
        id: 2,
        disable: false,
        value: 'FOR_COMPANIES'
    },
    {
        title: 'Native',
        description: 'Mobile or Desktop, apps that run natively in a device.',
        technology: 'e.g.: iOS SDK',
        id: 3,
        disable: true,
        value: 'FOR_EMPLOYEES'
    }
];
