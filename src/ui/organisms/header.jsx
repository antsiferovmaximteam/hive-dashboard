import React, {Component} from 'react';
import styled from 'styled-components';
import {Link} from 'react-router-dom';

import searchIcon from 'assets/images/search.svg';
import avatarArrowIcon from 'assets/images/avatar-arrow.svg';
import defaultAfatar from 'assets/images/avatar.svg';
import {NotificationsIcon} from 'assets/images/notifications-icon';

const HeaderWrapper = styled('header')`
  display: flex;
  align-items: center;
  padding-left: 44px;
`;

const HeaderNav = styled('ul')`
  display: inline-flex;
  margin-left: auto;
  list-style: none;
  
  li {
    display: flex;
    align-items: center;
    padding: 15px 12px;
    font-size: 12px;
    color: #504A8E;
    font-weight: 600;
    letter-spacing: 0;
    line-height: 12px;
  }
`;

const Search = styled('div')`
  position: relative;

  input {
    color: #504A8E;
    letter-spacing: 0;
    text-align: left;
    line-height: 18px;
    font-size: 12px;
    background: transparent;
    outline: none;
    border: none;
  
    &::placeholder {
      color: #AEB6DC;
    }
  }
  
  &:before {
    content: '';
    position: absolute;
    display: block;
    height: 14px;
    width: 14px;
    top: 4px;
    left: -16px;
    background-size: cover;
    background-image: url(${searchIcon});
  }
`;

const ProfileNav = styled('li')`
  padding-top: 0 !important;
  padding-bottom: 0 !important;
`;

const UserPhoto = styled('img')`
  width: 28px;
  border-radius: 50%;
`;

const UserName = styled('span')`
  padding: 0 19px 0 8px;
  position: relative;
  
  &:after {
    content: '';
    display: block;
    position: absolute;
    right: 0;
    top: calc(50% - 2px);
    width: 7px;
    height: 4px;
    background-image: url(${avatarArrowIcon});
  }
`;

export class Header extends Component {
    render() {
        return (
            <HeaderWrapper>
              <Search>
                <input type="text" placeholder='Type in to Search…'/>
              </Search>
              
              <HeaderNav>
                <li><Link to='/notifications'><NotificationsIcon/></Link></li>
                <li><Link to='/support'>Help & Support</Link></li>
                <li><Link to='/documentation'>Documentation</Link></li>
                <ProfileNav onClick={this.props.logout}>
                  <UserPhoto src={this.props.avatar || defaultAfatar}/>
                  <UserName>{this.props.name}</UserName>
                </ProfileNav>
              </HeaderNav>
            </HeaderWrapper>
        );
    }
}