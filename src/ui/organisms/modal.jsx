import React, {Component} from 'react';
import ReactModal from 'react-modal';
import { injectGlobal } from 'styled-components';

ReactModal.setAppElement('#root');

injectGlobal`
  .modal-content {
    position: relative;
    display: flex;
    justify-content: center;
    width: 100%;
    max-width: 785px;
    margin: 20px 15px;
    background: rgb(255, 255, 255);
    overflow-y: auto;
    border-radius: 2px;
    outline: none;
    box-shadow: 0 15px 26px 0 rgba(50,50,93,0.06), 0 0 11px 0 rgba(0,0,0,0.07);
    -webkit-overflow-scrolling:touch;
  }
  
  .modal-overlay {
    position: fixed;
    display: flex;
    justify-content: center;
    align-items: center;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 1001;
    background-color: rgba(255, 255, 255, 0.85);
    overflow-y: auto;
	-webkit-overflow-scrolling: touch;
  }
`;

class Modal extends Component {
    render() {
        return (
            <ReactModal
                isOpen={this.props.isOpen}
                onRequestClose={this.props.onClose}
                className={"modal-content " + this.props.classContent}
                overlayClassName={"modal-overlay " + this.props.classOverlay}
            >
                {this.props.children}
            </ReactModal>
        );
    }
}

Modal.defaultProps = {
    classContent: '',
    classOverlay: ''
};

export {Modal};
