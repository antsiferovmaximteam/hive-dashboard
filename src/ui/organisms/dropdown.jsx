import React, {Component} from 'react';

export class Dropdown extends Component {
    constructor() {
        super();

        this.dropdown = null;
    }

    state = {
      animationDuration: 500
    };

    componentDidMount() {
        if (!this.props.dropped) {
            this.dropdown.style.height = 0;
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.dropped !== nextProps.dropped) {
            if (nextProps.dropped) {
                this.dropdown.style.transitionDuration = 0;
                this.dropdown.style.height = 'auto';

                const height = this.dropdown.clientHeight;

                this.props.animating(true);
                this.dropdown.style.height = 0;
                this.dropdown.style.transitionDuration = this.state.animationDuration + 'ms';
                setTimeout(() => this.dropdown.style.height = height + 'px', 0);
                setTimeout(() => this.props.animating(false), this.state.animationDuration + 100);
            } else {
                this.props.animating(true);
                this.dropdown.style.height = 0;
                setTimeout(() => this.props.animating(false), this.state.animationDuration + 100);
            }
        }
    }

    render() {
        return (
            <this.props.wrapper innerRef={el => this.dropdown = el}>
                {this.props.children}
            </this.props.wrapper>
        );
    }
}