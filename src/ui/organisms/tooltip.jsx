import React, {Component} from 'react';
import styled from 'styled-components';

const TooltipWrapper = styled('div')`
  position: relative;
  display: inline-block;
`;

const TooltipContainer = styled('div')`
  position: absolute;
  display: ${p => p.show ? 'block' : 'none'};
  opacity: ${p => p.opacity};
  top: -34px;
  left: 50%;
  transform: translateX(-50%);
  box-shadow: 0 2px 4px 0 rgba(0,0,0,0.14);
  font-weight: 600;
  font-size: 12px;
  color: #3E466D;
  line-height: 18px;
  border-radius: 2px;
  transition: opacity ${p => p.duration}s;
  
  &:before {
    content: '';
    position: absolute;
    display: block;
    width: 12px;
    height: 12px;
    bottom: -3px;
    left: calc(50% - 6px);
    transform: rotate(45deg);
    box-shadow: 0 2px 4px 0 rgba(0,0,0,0.14);
    background-color: #fff;
    border-radius: 2px;
    z-index: 1;
  }
`;

const TooltipContent = styled('div')`
  position: relative;
  width: 100%;
  height: 100%;
  padding: 3px 12px;
  z-index: 100;
  background-color: #fff;
  border-radius: 3px;
`;

export class Tooltip extends Component {
    constructor() {
        super();

        this.timer = null;
        this.tooltip = null;
    }

    state = {
        show: false,
        opacity: 0,
        duration: .2
    };

    onFocus = () => {
        clearTimeout(this.timer);
        this.setState({show: true});

        setTimeout(() => this.setState({opacity: 1}), 10);
    };

    onBlur = () => {
        this.setState({opacity: 0});

        this.timer = setTimeout(() => this.setState({show: false}), this.state.duration * 1000)
    };

    render() {
        return (
            <TooltipWrapper onMouseEnter={this.onFocus} onMouseLeave={this.onBlur}>
                {this.props.children}
                <TooltipContainer
                    show={this.state.show}
                    opacity={this.state.opacity}
                    duration={this.state.duration}
                >
                    <TooltipContent>
                        {this.props.title}
                    </TooltipContent>
                </TooltipContainer>
            </TooltipWrapper>
        );
    }
}