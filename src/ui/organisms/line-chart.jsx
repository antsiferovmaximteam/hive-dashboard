import React, {Component} from 'react';
import PropTypes from 'prop-types';
import RechartsLineChart from 'recharts/lib/chart/LineChart';
import Line from 'recharts/lib/cartesian/Line';
import XAxis from 'recharts/lib/cartesian/XAxis';
import YAxis from 'recharts/lib/cartesian/YAxis';
import CartesianGrid from 'recharts/lib/cartesian/CartesianGrid';
import Tooltip from 'recharts/lib/component/Tooltip';
import styled, {injectGlobal} from "styled-components";

injectGlobal`
  .recharts-cartesian-axis-tick-value {
    font-size: 12px;
    fill: #3E466D;
    line-height: 18px;
 }
 
 .dashed-line {
  stroke-dasharray: 5;
 }
`;

const ChartWrapper = styled('div')``;

const TooltipWrapper = styled('div')`
  display: flex;
  flex-direction: column;
  align-items: center;
  position: absolute;
  top: 0;
  left: 0;
  width: 100px;
  font-weight: 600;
  font-size: 12px;
  color: rgba(255, 255, 255, .8);
  background-color: #6515DD;
  padding: 10px 0;
  border-radius: 3px;
  opacity: 0;
  transition: all .2s;
  
  strong {
      font-weight: bold;
      font-size: 16px;
      color: #FFFFFF;
      margin-bottom: 7px;
  }
`;

export class LineChart extends Component {
    constructor() {
        super();

        this.chartContainer = null;
    }

    state = {
        chartWidth: 0
    };

    componentDidMount() {
        this.updateChartWidth();
        window.addEventListener('resize', this.updateChartWidth, false)
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateChartWidth)
    }

    updateChartWidth = () => {
        this.setState({
            chartWidth: this.chartContainer.clientWidth,
        });
    };

    hideTooltip = () => {
        this.tooltip.style.transform = `translate(0px, 0px)`;
        this.tooltip && (this.tooltip.style.opacity = '0');
    };

    onMouseMove = (data) => {
        if (data.isTooltipActive) {
            const point = this.line.props.points[data.activeTooltipIndex];

            if (this.point !== point) {
                this.point = point;
            }
        }
        this.updateTooltip();
    };

    onChartMouseLeave = () => {
        this.point = null;
        this.updateTooltip();
    };

    updateTooltip = () => {
        if (this.props.tooltip && this.point && this.point.payload && this.tooltip && this.point.payload.data) {
            let x = Math.round(this.point.x);

            this.tooltip.style.opacity = '1';
            this.tooltip.style.transform = `translate(${x}px, -20px)`;

            this.tooltip.innerHTML = this.props.tooltip(this.point.payload);
        }
        else {
            this.tooltip.style.opacity = '0';
        }
    };

    render() {
        const {stroke = '#6743DF'} = this.props;

        return (
            <ChartWrapper tabIndex="1" innerRef={el => this.chartContainer = el} onBlur={this.hideTooltip}>
                {this.state.chartWidth !== 0 && (
                    [
                        <RechartsLineChart key="chart" width={this.state.chartWidth} height={this.props.height}
                                           data={this.props.data}
                                           margin={{top: 5, right: 30, left: 20, bottom: 5}}
                                           onMouseMove={this.onMouseMove} onMouseLeave={this.onChartMouseLeave}
                        >
                            <XAxis
                                dataKey="period"
                                type="number"
                                axisLine={false}
                                tickLine={false}
                                domain={this.props.xAxisDomain}
                                tickFormatter={this.props.xAxisTickFormatter}
                                ticks={this.props.xAxisTicks}
                            />
                            {this.props.xAxis && (
                                <YAxis
                                    axisLine={false}
                                    tickLine={false}
                                    padding={{top: 10}}
                                />
                            )}
                            {this.props.grid && (
                                <CartesianGrid
                                    horizontalFill={["rgba(103, 67, 223, 0.03)"]}
                                    stroke="rgba(149, 160, 210, 0.1)"
                                />
                            )}
                            <Tooltip
                                isAnimationActive={false}
                                active={false}
                                wrapperStyle={{opacity: 0}}
                                cursor={{stroke: '#FFB300', opacity: 0.5}}
                            />
                            <Line
                                ref={el => this.line = el}
                                type="natural"
                                dataKey="data"
                                stroke={stroke}
                                activeDot={{r: 7}} dot={false}
                                isAnimationActive={false}
                            />
                            {this.props.children}
                        </RechartsLineChart>,
                        <TooltipWrapper key='toooltip' innerRef={el => this.tooltip = el}/>
                    ]
                )}
            </ChartWrapper>
        );
    }
}

LineChart.defaultProps = {
    xAxis: true,
    grid: true,
    height: 300,
    tooltip: undefined
};

LineChart.propTypes = {
    xAxis: PropTypes.bool,
    grid: PropTypes.bool,
    height: PropTypes.number,
    tooltip: PropTypes.func,
    xAxisTicks: PropTypes.array,
    xAxisTickFormatter: PropTypes.func,
    xAxisDomain: PropTypes.array
};
