import React, {Component} from 'react';
import styled from 'styled-components';
import {Link, withRouter} from 'react-router-dom';

import {
    DashboardIcon,
    ApplicationsIcon,
    LogsIcon,
    FraudPreventionIcon,
    AnalyticsIcon,
    AccountIcon
} from 'assets/images/nav-icons';

import {Dropdown} from './dropdown';

const NavList = styled('ul')`
  position: relative;
  list-style: none;
  
  .nav-link {
    display: flex;
    align-items: center;
    font-size: 14px;
    color: #504A8E;
    line-height: 22px;
    font-weight: 600;
    transition: all .2s ease-in-out;
    
    &:hover {
      color: #504A8E;
    }
  }
`;

const Nav = styled('div')`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  padding: 8px 0 8px 8px;
`;

const NavItem = styled('li')`
  .nav-link {
    ${p => p.active && `
      color: #6515DD;
      
      &:hover {
        color: #6515DD;
      }
    `}
  }
`;

const NavIndicator = styled('li')`
  position: absolute;
  left: 0;
  top: 7px;
  width: 2px;
  height: 22px;
  border-radius: 1.5px;
  background-color: #6515DD;
  transition: all .4s ease-in-out;
`;

const Delimiter = styled('li')`
  height: 1px;
  width: 100%;
  background-color: #ECF0FF;
  margin: 8px 0;
`;

const DropdownWrapper = styled('ul')`
  list-style: none;
  margin: 5px 0 0 30px;
  padding: 0;
  overflow: hidden;
  transition: all .5s ease-in-out;
  
  a {
    display: block;
    padding: 4px 0;
    font-weight: 400;
    font-size: 12px;
    color: #504A8E;
    letter-spacing: 0;
    line-height: 18px;
    
    &:hover {
      color: #504A8E;
    }
  }
`;

const DropdownItem = styled('li')`
  a {
    ${p => p.active && 'color: #6515DD !important;'}
  }
`;

class NavigationUI extends Component {
    state = {
        indicatorTopOffset: 9,
        indicatorIsWait: false
    };

    componentDidMount() {
        this.drawIndicator();
    }

    componentDidUpdate() {
        setTimeout(() => (!this.state.indicatorIsWait && this.drawIndicator()), 10)
    }

    getCurrentPath = () => {
        return navItems.filter(item => this.isNavActive(item.path))[0].path
    };

    isNavActive = (path) => {
        if (path === '/' && path !== this.props.location.pathname) {
            return false;
        }
        
        return this.props.location.pathname.indexOf(path) >= 0;
    };

    drawIndicator = () => {
        const navLink = this.navListElement.querySelector(`[href="${this.getCurrentPath()}"]`);

        if (this.navListElement && navLink) {
            const top = navLink.parentNode.offsetTop;

            this.indicator.style.top = (top + this.state.indicatorTopOffset) + "px";
        }
    };

    indicatorWaiting = (isWait) => {
        this.setState({indicatorIsWait: isWait});
    };

    render() {
        return (
            <NavList innerRef={el => this.navListElement = el}>
                {navItems.map(item => (
                    [
                        item.delimiter && <Delimiter key='delimiter'/>,

                        <NavItem
                            key={item.path}
                            data-active={this.isNavActive(item.path) + ''}
                            active={this.isNavActive(item.path)}
                        >
                            <Nav>
                                <Link className='nav-link' to={item.path}>
                                    <item.icon/>
                                    <span>{item.title}</span>
                                </Link>
                                {item.children && (
                                    <Dropdown
                                        dropped={this.isNavActive(item.path) && isDropped(this.props.location.pathname, item.path)}
                                        wrapper={DropdownWrapper}
                                        animating={this.indicatorWaiting}
                                    >
                                        {item.children.map(child => (
                                            <DropdownItem active={isChildActive(this.props.location.pathname, child.path)} key={child.path}>
                                                <Link to={getChildPath(this.props.location.pathname, item.path, child.path)}>{child.title}</Link>
                                            </DropdownItem>
                                        ))}
                                    </Dropdown>
                                )}
                            </Nav>
                        </NavItem>]
                ))}
                <NavIndicator innerRef={el => this.indicator = el}/>
            </NavList>
        );
    }
}

function isChildActive(path, childPath) {
    const chunks = path.split('/');

    return chunks[chunks.length - 1] === childPath;
}

function isDropped(path, parentPath) {
    return !!path.split(parentPath).filter(i => i !== '')[0];
}

function getId(path) {
    const res = /applications\/(.+)\//.exec(path);

    return res !== null ? res[1] : '';
}

function getChildPath(path, parentPath, childPath) {
    return `${parentPath}/${getId(path)}/${childPath}`
}

const navItems = [
    {
        title: 'Dashboard',
        icon: DashboardIcon,
        path: '/'
    },
    {
        title: 'Applications',
        icon: ApplicationsIcon,
        path: '/applications',
        children: [
            {
                title: 'Settings',
                path: 'settings',
            },
            {
                title: 'Scopes',
                path: 'scopes',
            },
            {
                title: 'Remove',
                path: 'remove',
            }
        ]
    },
    {
        title: 'Logs',
        icon: LogsIcon,
        path: '/logs'
    },
    {
        title: 'Fraud Prevention',
        icon: FraudPreventionIcon,
        path: '/fraud-prevention'
    },
    {
        title: 'Analytics',
        icon: AnalyticsIcon,
        path: '/analytics'
    },
    {
        title: 'Account',
        icon: AccountIcon,
        path: '/account',
        delimiter: true
    },
];

const Navigation = withRouter(NavigationUI);

export {Navigation};
