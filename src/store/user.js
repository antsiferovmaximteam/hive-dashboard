import {createActions, handleActions} from 'redux-actions';
import {Api} from 'api';

export const {getUser} = createActions({
    GET_USER: async () => {
        const {data} = await Api.getUser();

        return data;
    }
});

export const reducer = handleActions({
    [getUser]: (state, {payload}) => ({...state, ...payload}),
}, {});