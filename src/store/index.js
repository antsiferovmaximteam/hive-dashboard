import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import { createLogger } from 'redux-logger';
import promiseMiddleware from 'redux-promise';

import {reducer as authReducer} from 'features/auth/store';
import {reducer as accountReducer} from 'features/account/store';
import {reducer as appsReducer} from 'features/applications/store';
import {reducer as userReducer} from './user';

const reducer = combineReducers({
    auth: authReducer,
    account: accountReducer,
    apps: appsReducer,
    user: userReducer,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export function configureStore(initialState = {}) {
    const middlewares = [
        createLogger({ collapsed: true }),
        promiseMiddleware,
    ];

    return createStore(
        reducer,
        initialState,
        composeEnhancers(applyMiddleware(...middlewares))
    )
}